package battleship.exceptions;

public class InvalidShipParametersException extends Exception {

    @Override
    public String getMessage() {
        return ExceptionMessages.INVALID_SHIP_PARAMETERS_MSG;
    }
    
    
    
}

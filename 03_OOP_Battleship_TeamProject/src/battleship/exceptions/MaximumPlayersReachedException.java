package battleship.exceptions;

public class MaximumPlayersReachedException extends Exception {

    @Override
    public String getMessage() {
        return ExceptionMessages.MAXIMUM_PLAYERS_REACHED_MSG;
    }
    
    
    
}

package battleship.exceptions;

public abstract class ExceptionMessages {
    
    public static final String INVALID_SHIP_PARAMETERS_MSG = "A hajó mérete nem megfelelő. A hajó nem hozható létre.";
    public static final String MAXIMUM_PLAYERS_REACHED_MSG = "Nem adható hozzá több játékos (max. 2)!";
    
}

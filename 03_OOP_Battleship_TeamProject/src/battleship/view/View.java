package battleship.view;

import battleship.controller.Controller;
import battleship.model.Map;
import battleship.model.MapTile;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import battleship.view.MapButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class View extends JFrame implements ActionListener {
    
    private Controller controller;
    
    private JPanel pnl;

    private JPanel titlePanel = new JPanel(new BorderLayout());
    private JPanel playerPanel = new JPanel(new BorderLayout());
    private JPanel computerPanel = new JPanel(new BorderLayout());
    private JPanel playerMap = new JPanel(new GridLayout(10, 10));
    private JPanel computerMap = new JPanel(new GridLayout(10, 10));
    
    private JTextArea pText = new JTextArea(">> Player's board");
    private JTextArea cText = new JTextArea(">> Computer's board");
    
    private MapButton[][] playerButtons = new MapButton[10][10];
    private MapButton[][] computerButtons = new MapButton[10][10];
    
    public View() throws HeadlessException {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1500, 500);
        setLocationRelativeTo(this);
        setLayout(new GridLayout(1, 2));
        setAlwaysOnTop(true);
        setResizable(false);

        add(playerPanel);
        add(computerPanel);

        pnl = (JPanel) getContentPane();

//        pLabel.add(playerPanel, BorderLayout.NORTH);
//        cLabel.add(computerPanel, BorderLayout.NORTH);
        playerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        computerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        playerPanel.add(playerMap, BorderLayout.NORTH);
        computerPanel.add(computerMap, BorderLayout.NORTH);
        playerMap.setPreferredSize(new Dimension(500, 500));
       // playerMap.setBorder(BorderFactory.createLineBorder(Color.yellow, 3));
        computerMap.setPreferredSize(new Dimension(500, 500));

        pText.setPreferredSize(new Dimension(500, 70));
        cText.setPreferredSize(new Dimension(500, 70));
        pText.setForeground(new java.awt.Color(0, 51, 102)); //Button miatt nem kell?
        cText.setForeground(new java.awt.Color(204, 0, 0));  //Button miatt nem kell?

        cText.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        pText.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        pText.setBackground(new java.awt.Color(255, 255, 204));
        cText.setBackground(new java.awt.Color(255, 255, 204));

        Font font = new Font("Arial", Font.PLAIN, 18);
        pText.setFont(font);
        cText.setFont(font);

        playerPanel.add(pText, BorderLayout.CENTER);
        computerPanel.add(cText, BorderLayout.CENTER);
        playerPanel.setBackground(new Color(153, 153, 255));
        playerPanel.setBackground(new Color(153, 204, 255));

        computerPanel.setBackground(new java.awt.Color(255, 153, 153));

        pack();
        
    }
    
    public void drawComputerMap(MapTile[][] mapData){
        for (int col = 0; col < 10; col++) {
            for (int row = 0; row < 10; row++) {
                computerButtons[row][col] = new MapButton();
                computerButtons[row][col].setCoordinates(row, col);
                this.setColorForMapButton(mapData[row][col].getState(), computerButtons[row][col]);
                computerMap.add(computerButtons[row][col]);
                

                // computerButtons[col][row].setBorder(BorderFactory.createBevelBorder(0));
                computerButtons[row][col].setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));

                computerButtons[row][col].addActionListener(this);
                
                cText.setText(">> player's board, 0 Shots");

            }
        }
    }
    
    public void drawPlayerMap(MapTile[][] mapData){
        for (int col = 0; col < 10; col++) {
            for (int row = 0; row < 10; row++) {
                playerButtons[row][col] = new MapButton();
                playerButtons[row][col].setCoordinates(row, col);
                this.setColorForMapButton(mapData[row][col].getState(), playerButtons[row][col]);
                playerMap.add(playerButtons[row][col]);
                //playerButtons[row][col].addActionListener(this);
                              
                playerButtons[row][col].setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
                //playerButtons[row][col].setEnabled(false);
            }
        }
    }
    
    public void refreshComputerMap(MapTile[][] mapData){
        for (int col = 0; col < 10; col++) {
            for (int row = 0; row < 10; row++) {
                this.setColorForMapButton(mapData[row][col].getState(), computerButtons[row][col]);
            }
        }
    }

    public void refreshPlayerMap(MapTile[][] mapData){
        for (int col = 0; col < 10; col++) {
            for (int row = 0; row < 10; row++) {
                this.setColorForMapButton(mapData[row][col].getState(), playerButtons[row][col]);
                
            }
        }
    }
    
    public void setColorForMapButton(int state, MapButton mb){
        switch(state){
            case Map.WATER_HIT: this.setButtonToWaterHit(mb); break;
            case Map.WATER_HIDDEN: this.setButtonToWaterHidden(mb); break; // 0 50 200
            case Map.WATER: this.setButtonToWater(mb); break;
            case Map.SHIP_HIT: setButtonToShipHit(mb); break;
            case Map.SHIP_HIDDEN: this.setButtonToShipHidden(mb); break;
        }
    }
    
    public void setController(Controller controller){
        this.controller = controller;
    }
    
    public void render(){
        setVisible(true);
    }
    
    public void showPlayerEventMessage(String msg){
        this.pText.setText(">> " + msg + "\n" + this.pText.getText());
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (!controller.isGameOver()) {
            MapButton mb = (MapButton) e.getSource();
            mb.removeActionListener(this);
            controller.performAction(mb.getCoordinates());
        } else {
            this.showMessage("Vége! " + controller.getWinner().getName() + " nyerte a játékot!");
        }
    }
    
    private void setButtonToShipHidden(MapButton mb) {
        mb.setBackground(new Color(0, 45, 175)); // dark blue
    }
    
    private void setButtonToWaterHidden(MapButton mb) {
        mb.setBackground(new Color(0, 45, 175)); // dark blue
    }
    
    private void setButtonToWater(MapButton mb) {
        mb.setBackground(new Color(85, 130, 255)); // light blue
    }

    private void setButtonToWaterHit(MapButton mb) {
        mb.setBackground(new Color(85, 130, 255)); // light blue
        mb.setForeground(new Color(255, 255, 255));
        mb.setFont(new Font("Arial", Font.BOLD, 30));
        mb.setText("O");
        mb.setOpaque(true);
    }

    private void setButtonToShip(MapButton mb) {
        mb.setBackground(Color.YELLOW);
        mb.setForeground(Color.RED);
        mb.setFont(new Font("Arial", Font.BOLD, 30));
        mb.setMargin(new Insets(0, 0, 0, 0));
        //      mb.setHorizontalAlignment(Cent  er);
        mb.setIcon(null);
        mb.setText("X");
        mb.setOpaque(true);
    }

    private void setButtonToShipHit(MapButton mb) {
        mb.setBackground(new Color(255, 100, 0)); // dark orange
        mb.setForeground(new Color(25, 25, 25)); //bright red
        mb.setFont(new Font("Arial", Font.BOLD, 30));
        mb.setMargin(new Insets(0, 0, 0, 0));
        mb.setIcon(null);
        mb.setText("X");
        mb.setOpaque(true);
    }

    public void showMessage(String msg) {
        JOptionPane.showMessageDialog(pnl, msg);
    }
}

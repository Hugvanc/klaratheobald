package battleship.view;

import battleship.model.Coordinates;
import javax.swing.JButton;

public class MapButton extends JButton {
    
    private Coordinates coordinates;
    
    public MapButton(){
        super();
        this.coordinates = new Coordinates();
    }
    
    public MapButton(String str){
        super(str);
        this.coordinates = new Coordinates();
    }
    
    public Coordinates getCoordinates(){
        return this.coordinates;
    }
    
    public void setCoordinates(int x, int y){
        this.setXCoordinate(x);
        this.setYCoordinate(y);
    }
    
    public int getXCoordinate() {
        return this.coordinates.getX();
    }

    public void setXCoordinate(int x) {
        this.coordinates.setX(x);
    }

    public int getYCoordinate() {
        return this.coordinates.getY();
    }

    public void setYCoordinate(int y) {
        this.coordinates.setY(y);
    }
    
    
    
}

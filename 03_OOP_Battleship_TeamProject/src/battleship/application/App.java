package battleship.application;

import battleship.controller.Controller;
import battleship.model.Model;
import battleship.view.View;

public class App {
    
    public App(){
        Model model = new Model();
        View view = new View();
        Controller controller = new Controller(model, view);
    }
    
}

package battleship.controller;

import battleship.model.Coordinates;
import battleship.model.Map;
import battleship.model.MapFactory;
import battleship.model.Player;
import battleship.model.Ship;
import battleship.utils.Config;
import java.util.ArrayList;
import java.util.List;

public class GameController {
    
    private List <Player> players;
    private Player currentPlayer;
    private Player targetPlayer;
    private Player winner;
    
    public GameController(){
        this.players = new ArrayList<>();
        this.currentPlayer = null;
        this.winner = null;
    }

    public Player getTargetPlayer() {
        return targetPlayer;
    }

    public void setTargetPlayer(Player targetPlayer) {
        this.targetPlayer = targetPlayer;
    }
    
    public Player getWinner() {
        return this.winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }
    
    public void addNewPlayer(Player player) {
        if (this.players.size() != Config.MAX_PLAYERS_COUNT){
            this.players.add(player);
        }
    }
    
    public Player getPlayer(int index){
        return this.players.get(index);
    }
    
    public Player getCurrentPlayer(){
        return this.currentPlayer;
    }
    
    public void setCurrentPlayer(Player player){
        this.currentPlayer = player;
    }
    
    public void changePlayer(){
        if (this.currentPlayer == this.getPlayer(0)) {
            this.setCurrentPlayer(this.getPlayer(1));
            this.setTargetPlayer(this.getPlayer(0));
            System.out.println("Changed player! Current:" + this.getPlayer(1).getName() + " Target: " + this.getPlayer(0).getName());
        } else {
            this.setCurrentPlayer(this.getPlayer(0));
            this.setTargetPlayer(this.getPlayer(1));
            System.out.println("Changed player! Current:" + this.getPlayer(0).getName() + " Target: " + this.getPlayer(1).getName());
        }
    }
    
    public void placeShips(Player player){
        ArrayList<Ship> ships = (ArrayList<Ship>)player.getShips();
        Map map = player.getMap();
        
        MapFactory mf = new MapFactory();
        mf.createAndPlaceShips(ships, map);
    }
        
    public boolean isGameOver(){
        if (this.getPlayer(0).isDefeated()) {
            this.setWinner(this.getPlayer(1));
            return true;
        } else if (this.getPlayer(1).isDefeated()) {
            this.setWinner(this.getPlayer(0));
            return true;
        }
        return false;
    }
    
    public Coordinates getComputerRandomShoot(Map map){
        int x, y;
        
        do {
            y = (int) (Math.random() * 10);
            x = (int) (Math.random() * 10);
        } while (map.getMapTile(x, y).getState() == Map.WATER_HIT 
                || map.getMapTile(x, y).getState() == Map.SHIP_HIT 
                || map.getMapTile(x, y).getState() == Map.WATER);
        
        Coordinates coordinates = new Coordinates(x, y);
        
        if (map.getMapTile(x, y).getState() == Map.WATER_HIDDEN) {
            map.getMapTile(x, y).setState(Map.WATER_HIT);
        } else {
            map.getMapTile(x, y).setState(Map.SHIP_HIT);
        }

        return coordinates;
    }
    
    public void setHiddenWaterVisibleAroundShip(Coordinates coordinates, Map map) {
        int x = coordinates.getX();
        int y = coordinates.getY();
        
        if (map.getMapTile(x, y).getState() == Map.SHIP_HIT) {
            map.getMapTile( Math.abs(x - 1), Math.abs(y - 1) ).setState(Map.WATER);
            map.getMapTile( Map.SIZE- 1 - Math.abs(Map.SIZE - 2 - x), Math.abs(y - 1)).setState(Map.WATER);
            map.getMapTile( Math.abs(x - 1), Map.SIZE - 1 - Math.abs(Map.SIZE - 2 - y)).setState(Map.WATER);
            map.getMapTile( Map.SIZE - 1 - Math.abs(Map.SIZE - 2 - x), Map.SIZE - 1 - Math.abs(Map.SIZE - 2 - y))
                .setState(Map.WATER);
        }
    }
    
}

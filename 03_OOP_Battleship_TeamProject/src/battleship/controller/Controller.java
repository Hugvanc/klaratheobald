package battleship.controller;

import battleship.exceptions.MaximumPlayersReachedException;
import battleship.model.Coordinates;
import battleship.model.MapFactory;
import battleship.model.MapTile;
import battleship.model.Player;
import battleship.model.Model;
import battleship.model.Ship;
import battleship.model.ShipTile;
import battleship.view.View;
import java.util.List;

public class Controller {
    
    private Model model;
    private View view;
    private GameController gameController = new GameController();
    
    public Controller(Model model, View view){
        this.model = model;
        this.view = view;
        this.view.setController(this);
        
        Player player1 = new Player("PLAYER_1_HUMAN");
        Player player2 = new Player("PLAYER_2_PC");
        gameController.addNewPlayer(player1);
        gameController.addNewPlayer(player2);
            
        // place their ships
        gameController.placeShips(player1);
        gameController.placeShips(player2);
            
        // game loop
        gameController.setCurrentPlayer(player1); // HUMAN starts
        gameController.setTargetPlayer(player2);  // PC is the target
        
        view.drawPlayerMap(player1.getMap().getMapData());
        view.drawComputerMap(player2.getMap().getMapData());
        
        view.render();
    }
    
    public MapTile[][] getPlayerMapData(int index){
        return gameController.getPlayer(index).getMap().getMapData();
    }
    
    public boolean isGameOver(){
        return gameController.isGameOver();
    }
    
    public Player getWinner(){
        return gameController.getWinner();
    }
    
    public void performAction( Coordinates coordinates){
        if (!gameController.isGameOver()) {
            //System.out.println("curent player: " + gameController.getCurrentPlayer().getName());
            
            gameController.getPlayer(1).getMap().getMapTile(coordinates.getX(), coordinates.getY() ).hit();
            gameController.setHiddenWaterVisibleAroundShip(coordinates, gameController.getPlayer(1).getMap());
            
            gameController.changePlayer();
            
            Coordinates coordinatesComp = gameController.getComputerRandomShoot(gameController.getPlayer(0).getMap());
            gameController.getPlayer(0).getMap().getMapTile( coordinatesComp ).hit();
            gameController.setHiddenWaterVisibleAroundShip(coordinatesComp, gameController.getPlayer(0).getMap());
            
            view.refreshComputerMap(gameController.getPlayer(1).getMap().getMapData());
            view.refreshPlayerMap(gameController.getPlayer(0).getMap().getMapData());
        } else {
            System.out.println("GAME OVER! Winner: " + gameController.getWinner().getName());
        }
    }
    
}

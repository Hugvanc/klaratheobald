package battleship.utils;

public abstract class Config {
    
    // Map constants
    public static final int MAP_SIZE = 10;
    
    // Player constants
    public static final int MAX_PLAYERS_COUNT = 2;
    
    // Ship constants
    public static final int MIN_SHIP_SIZE = 1;
    public static final int MAX_SHIP_SIZE = 4;
    public static final int SHIP_MAX_COUNT = 10;
    public static final int SHIP_CARRIER_COUNT = 1;
    public static final int SHIP_BATTLESHIP_COUNT = 2;
    public static final int SHIP_DESTROYER_COUNT = 3;
    public static final int SHIP_SUBMARINE_COUNT = 4;
    
    public static final int[] PLAYER_SHIP_SET = {SHIP_CARRIER_COUNT, SHIP_BATTLESHIP_COUNT, SHIP_DESTROYER_COUNT, SHIP_SUBMARINE_COUNT};
    
}

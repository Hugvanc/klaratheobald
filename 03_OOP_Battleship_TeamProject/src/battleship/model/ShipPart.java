package battleship.model;

public class ShipPart {
    
    private Ship parentShip;
    private Coordinates coordinates;
    private boolean isHit;
    
    public ShipPart(Ship ship){
        this.parentShip = ship;
        this.isHit = false;
    }
    
    public Ship getParentShip(){
        return this.parentShip;
    }
    
    public Coordinates getCoordinate(){
        return this.coordinates;
    }
    
    public void setCoordinates(Coordinates coordinates){
        this.coordinates = coordinates;
    }
    
    public boolean isHit(){
        return this.isHit;
    }
    
    public void setToHit(){
        //System.out.println("HIT CHAIN: in shipPart setToHit()");
        this.isHit = true;
        this.parentShip.incHits();
    }
    
}

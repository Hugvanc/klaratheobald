package battleship.model;

import java.util.ArrayList;

public class MapFactory {
    
    private int[] matrix = new int[100];
    private int dimenzio = 10;

    private int shipLength;

    private int sor;
    private int oszlop;

    private int orientation;

    private int farvíz = 8;
    private int shipSymbol = 1;
    
    public void createAndPlaceShips(ArrayList<Ship> ships, Map playerMap){
        this.FillWithWater();
        for (Ship ship : ships) {
            int[][] shipData = new int[ship.getSize()][2];
            if (ship.getSize()>1){
                shipData = this.CreateShip(ship.getSize());
            } else {
                shipData[0] = this.CreateSubmarine();
            }
            
            for (int i = 0; i < shipData.length; i++) {
                Coordinates coordinates = new Coordinates(shipData[i][1]-1, shipData[i][0]-1);
                //System.out.println("shipData(" + i + ") x: " + coordinates.getX() + " y: " + coordinates.getY());
                ShipPart shipPart = ship.getShipPart(i);
                shipPart.setCoordinates(coordinates);
                playerMap.setMapTile(coordinates, new ShipTile(coordinates, shipPart));
            }
        }
        this.ClearPlaceHolder();
    }
    
    // térkép feltöltése vízzel
    public void FillWithWater(){
        for (int i = 0; i < 100; i++) {
                matrix[i] = 0;
            }   
    }
    
    // farvíz eltüntetése
    public void ClearPlaceHolder(){
        for (int i = 0; i < 100; i++) {
            if(matrix[i] == 8){
                matrix[i] = 0;
            }
        }
    }
    
    
    // térkép kiírása konzolra
    public void PrintSeaMap(){
        System.out.format("%n");
        for (int i = 0; i < 100; i++) {
            System.out.format("%d ",matrix[i]);
            if (i == 9 || i == 19 || i == 29 || i == 39 || i == 49 || i == 59 || i == 69 || i == 79 || i == 89 || i == 99){
                System.out.format("%n");
            }
        }    
    }
    
    public int[] getRawMap(){
        return this.matrix;
    }
    
    
    // a: sor, b: oszlop
    public int getField(int a, int b){
        return this.matrix[((a-1)*10)+(b-1)];
    }
    
    // hajó elhelyezése 
    public int[][] CreateShip(int a){
        shipLength = a;
        int[][] coordinates = new int[a][2];
        int counter = 0;
        
        int y = 0;
        
        for (int x = 0; x < 20; x++) { // a 20 itt arbitrary number
            y++;
            x--;
            if(y==100){
                x = 20;
            }
        
            

            
            sor = 1 + (int)(Math.random() * ((10 - 1) + 1));
            oszlop = 1 + (int)(Math.random() * ((10 - 1) + 1));
            
            orientation = 1 + (int)(Math.random() * ((2-1)+1));

            

            if (matrix[((sor-1)*dimenzio)+(oszlop-1)] != 0){
//                System.out.format("%n A célterület nem szabad! %n");
            }
            if (orientation == 1 && oszlop > (shipLength-1) && matrix[((sor-1)*dimenzio)+(oszlop-1)] == 0){ // ez ellenőrzi, hogy elfér-e egyáltalán nyugat felé a hajó
                for (int i = 0; i < (shipLength-1); i++) {
                    if (matrix[((sor-1)*dimenzio)+(oszlop-2-i)] != 0){
                        i = shipLength-1; // véget ér a ciklus
//                        System.out.format("%n Nyugat felé nem fér el a hajó! %n");
                    }
                    else {
                        if (i == shipLength - 2){
                            for (int j = 0; j < shipLength; j++) {
                                matrix[((sor-1)*dimenzio)+(oszlop-1-j)] = shipSymbol; // elhelyezi a hajórészt
                                coordinates[counter][0] = sor;
                                coordinates[counter][1] = oszlop-j;
                                counter++; // elmenti a lerakott hajórész koordinátáit
                                
                                if(j == 0){
                                    try {
                                        matrix[((sor-2)*dimenzio)+(oszlop-j-1)] = farvíz;
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != 10){
                                            matrix[((sor-2)*dimenzio)+(oszlop-j)] = farvíz;
                                        }
                                        
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != 10){
                                            matrix[((sor-1)*dimenzio)+(oszlop-j)] = farvíz;
                                        }
                                        
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        matrix[((sor)*dimenzio)+(oszlop-j-1)] = farvíz;
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != 10){
                                            matrix[((sor)*dimenzio)+(oszlop-j)] = farvíz;
                                        }
                                        
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                }
                                if(j == (shipLength-1)){
                                    try {
                                        if(oszlop != shipLength){
                                            matrix[((sor-2)*dimenzio)+((oszlop-j)-2)] = farvíz;
                                        }
                                            
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                            matrix[((sor-2)*dimenzio)+((oszlop-j)-1)] = farvíz;
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != shipLength){
                                            matrix[((sor-1)*dimenzio)+((oszlop-j)-2)] = farvíz;
                                        }
                                            
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != shipLength){
                                            matrix[((sor)*dimenzio)+((oszlop-j)-2)] = farvíz;
                                        }
                                            
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                            matrix[((sor)*dimenzio)+((oszlop-j)-1)] = farvíz;
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                }
                                if(j != 0 && j != (shipLength-1)){
                                    try {
                                            matrix[((sor-2)*dimenzio)+((oszlop-j)-1)] = farvíz;
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                            matrix[((sor)*dimenzio)+((oszlop-j)-1)] = farvíz;
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                }
                            }
                                
                            
                            // Mátrix kiírása
                            x = 20; // ha hajó lerakódik, véget ér a próbálkozás
                            
//                            System.out.format("%n");
//                            for (int k = 0; k < 100; k++) {
//                                System.out.format("%d     ",matrix[k]);
//                                if (k == 9 || k == 19 || k == 29 || k == 39 || k == 49 || k == 59 || k == 69 || k == 79 || k == 89 || k == 99){
//                                    System.out.format("%n%n");
//                                }
//                            }
                            
                            }
                        }
                    }
                }
            
            if (oszlop < (shipLength) && matrix[((sor-1)*dimenzio)+(oszlop-1)] == 0){
//                System.out.format("%n Nyugat felé KILÓGNA a pályáról a hajó! %n");
            }
            
            if (sor > (shipLength-1) && matrix[((sor-1)*dimenzio)+(oszlop-1)] == 0){ // ez ellenőrzi, hogy elfér-e egyáltalán észak felé a hajó
                for (int i = 0; i < (shipLength-1); i++) {
                    if (matrix[(((sor-1-i)-1)*dimenzio)+(oszlop-1)] != 0){
                        i = shipLength-1; // véget ér a ciklus
//                        System.out.format("%n Észak felé nem fér el a hajó! %n");
                    }
                    else {
                        if (i == shipLength - 2){
                            for (int j = 0; j < shipLength; j++) {
                                matrix[(((sor-j)-1)*dimenzio)+(oszlop-1)] = shipSymbol;
                                coordinates[counter][0] = sor-j;
                                coordinates[counter][1] = oszlop;
                                counter++; // elmenti a lerakott hajórész koordinátáit
                                
                                if(j == 0){
                                    try {
                                        if(oszlop != 1){
                                            matrix[(((sor-j)-1)*dimenzio)+(oszlop-2)] = farvíz;
                                        }
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != 1){
                                            matrix[(((sor-j))*dimenzio)+(oszlop-2)] = farvíz;
                                        }
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        matrix[(((sor-j))*dimenzio)+(oszlop-1)] = farvíz;
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != 10){
                                            matrix[(((sor-j))*dimenzio)+(oszlop)] = farvíz;
                                        }
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != 10){
                                            matrix[(((sor-j)-1)*dimenzio)+(oszlop)] = farvíz;
                                        }
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }

                                }
                                if(j == (shipLength-1)){
                                    try {
                                        if(oszlop != 1){
                                            matrix[(((sor-j)-2)*dimenzio)+(oszlop-2)] = farvíz;
                                        }
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        matrix[(((sor-j)-2)*dimenzio)+(oszlop-1)] = farvíz;
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != 10){
                                            matrix[(((sor-j)-2)*dimenzio)+(oszlop)] = farvíz;
                                        }
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != dimenzio){
                                            matrix[(((sor-j)-1)*dimenzio)+(oszlop)] = farvíz;
                                        }
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != 1){
                                            matrix[(((sor-j)-1)*dimenzio)+(oszlop-2)] = farvíz;
                                        }
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                }
                                if(j != 0 && j != (shipLength-1)){
                                    try {
                                        if(oszlop != 10){
                                            matrix[(((sor-j)-1)*dimenzio)+(oszlop)] = farvíz;
                                        }
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                    try {
                                        if(oszlop != 1){
                                            matrix[(((sor-j)-1)*dimenzio)+(oszlop-2)] = farvíz;
                                        }
                                        }
                                    catch(ArrayIndexOutOfBoundsException ignored) {

                                        }
                                }
                            }
                            // Mátrix kiírása
                            x = 20; // ha hajó lerakódik, véget ér a próbálkozás
                            
//                            System.out.format("%n");
//                            for (int k = 0; k < 100; k++) {
//                                System.out.format("%d     ",matrix[k]);
//                                if (k == 9 || k == 19 || k == 29 || k == 39 || k == 49 || k == 59 || k == 69 || k == 79 || k == 89 || k == 99){
//                                    System.out.format("%n%n");
//                                }
//                            }
                            
                        }
                    }
                }
            }
            if (sor < (shipLength) && matrix[((sor-1)*dimenzio)+(oszlop-1)] == 0){
//                System.out.format("%n Észak felé KILÓGNA a pályáról a hajó! %n");
            }
        }
        
        
        
        return coordinates;
        
    }
    
    
    // tengeralattjáró (azaz 1x1-es hajó) elhelyezése
    public int[] CreateSubmarine(){
        int[] coordinates = new int[2];
        
        
        int y = 0;
        
        for (int x = 0; x < 20; x++) { // a 20 itt arbitrary number
            y++;
            x--;
            if(y==100){
                x = 20;
            }
        
            

            
            sor = 1 + (int)(Math.random() * ((10 - 1) + 1));
            oszlop = 1 + (int)(Math.random() * ((10 - 1) + 1));
            
            if (matrix[((sor-1)*dimenzio)+(oszlop-1)] == 0){
                
                matrix[((sor-1)*dimenzio)+(oszlop-1)] = shipSymbol; // elhelyezi a hajórészt
                coordinates[0] = sor;
                coordinates[1] = oszlop;
                
                try {
                    matrix[((sor-2)*dimenzio)+(oszlop-1)] = farvíz;
                    }
                catch(ArrayIndexOutOfBoundsException ignored) {

                    }
                try {
                    matrix[((sor)*dimenzio)+(oszlop-1)] = farvíz;
                    }
                catch(ArrayIndexOutOfBoundsException ignored) {

                    }
                try {
                    if(oszlop != 1){
                        matrix[((sor-1)*dimenzio)+(oszlop-2)] = farvíz;
                    }
                    
                    }
                catch(ArrayIndexOutOfBoundsException ignored) {

                    }
                try {
                    if(oszlop != 10){
                        matrix[((sor-1)*dimenzio)+(oszlop)] = farvíz;
                    }
                    
                    }
                catch(ArrayIndexOutOfBoundsException ignored) {

                    }
                try {
                    if(oszlop != 1){
                        matrix[((sor)*dimenzio)+(oszlop-2)] = farvíz;
                    }
                    
                    }
                catch(ArrayIndexOutOfBoundsException ignored) {

                    }
                try {
                    if(oszlop != 10){
                        matrix[((sor)*dimenzio)+(oszlop)] = farvíz;
                    }
                    
                    }
                catch(ArrayIndexOutOfBoundsException ignored) {

                    }
                try {
                    if(oszlop != 1){
                        matrix[((sor-2)*dimenzio)+(oszlop-2)] = farvíz;
                    }
                    matrix[((sor-2)*dimenzio)+(oszlop-2)] = farvíz;
                    }
                catch(ArrayIndexOutOfBoundsException ignored) {

                    }
                try {
                    if(oszlop != 10){
                        matrix[((sor-2)*dimenzio)+(oszlop)] = farvíz;
                    }
                    
                    }
                catch(ArrayIndexOutOfBoundsException ignored) {

                    }

                x = 20; // ha hajó lerakódik, véget ér a próbálkozás

            }
        
        }
            
        return coordinates;
    }
    
}

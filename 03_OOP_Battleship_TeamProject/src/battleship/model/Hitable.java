package battleship.model;

public interface Hitable {
    
    public void hit();
    
}

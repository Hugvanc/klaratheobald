package battleship.model;

public abstract class MapTile implements Hitable {
    
    public int state;
    public Coordinates coordinates;
    
    public int getState(){
        return this.state;
    }
    
    public void setState(int state){
        this.state = state;
    }
    
    public Coordinates getCoordinates(){
        return this.coordinates;
    }
    
}

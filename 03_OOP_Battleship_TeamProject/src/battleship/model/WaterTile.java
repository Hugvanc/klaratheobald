package battleship.model;

public class WaterTile extends MapTile {
    
    public WaterTile(){
        this.state = Map.WATER_HIDDEN;
    }
    
    @Override
    public void hit() {
        System.out.println("HIT CHAIN: WaterTile");
        this.setState(Map.WATER_HIT);
    }
    
}

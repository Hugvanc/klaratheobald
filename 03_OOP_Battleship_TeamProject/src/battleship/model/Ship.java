package battleship.model;

import battleship.exceptions.InvalidShipParametersException;
import battleship.utils.Config;
import com.sun.media.sound.JavaSoundAudioClip;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ship {
    
    private Player player;
    private final int size;
    private int hits;
    private boolean destroyed;
    private List<ShipPart> parts;
    
    public Ship(Player player, int size) throws InvalidShipParametersException {
        this.player = player;
        this.size = size;
        this.hits = 0;
        this.destroyed = false;
        this.parts = new ArrayList<>();
        if ( (this.size >= Config.MIN_SHIP_SIZE) & (this.size <= Config.MAX_SHIP_SIZE) ) {
            for (int i = 0; i < this.size; i++) {
                this.parts.add(new ShipPart(this));
            }
        } else {
            throw new InvalidShipParametersException();
        }
    }
    
    public void addShipPart(ShipPart shipPart){
        this.parts.add(shipPart);
    }
    
    public ShipPart getShipPart(int index){
        return this.parts.get(index);
    }
    
    public int getSize(){
        return this.size;
    }
    
    public int getHits(){
        return this.hits;
    }
    
    public void incHits(){
        this.hits++;
        //System.out.println("HIT CHAIN: ship.incHits() hits: "+this.size+"/"+this.hits);
        if (this.hits == this.size){
            this.setDestroyed();
        }
    }
    
    public boolean isDestroyed(){
        return this.destroyed;
    }
    
    public void setDestroyed(){
        this.destroyed = true;
        this.player.decShipCount();
        //System.out.println("HIT CHAIN: ship.setDestroyed() "+this.player.getName() + "\'s ship has destroyed!");
        try {
            new JavaSoundAudioClip(new FileInputStream(new File(".\\src\\battleship\\sound\\explosion.wav"))).play();
        } catch (Exception ex) {
            ;
        }
    }
    
}

package battleship.model;

public class Coordinates {
    
    private int col;
    private int y;
    
    public Coordinates(){};
    
    public Coordinates(int x, int y){
        this.col = x;
        this.y = y;
    }
    
    public int getX(){
        return this.col;
    }
    
    public int getY(){
        return this.y;
    }

    public void setX(int x) {
        this.col = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    
}

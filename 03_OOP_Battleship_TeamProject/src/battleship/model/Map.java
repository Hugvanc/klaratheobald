package battleship.model;

import java.util.ArrayList;
import java.util.List;

public class Map {
    
    public static final int SIZE = 10;
    public static final int WATER_HIDDEN = -1;
    public static final int WATER = 0;
    public static final int WATER_HIT = 3;
    public static final int SHIP_HIDDEN = 1;
    public static final int SHIP_HIT = 2;
    
    private MapTile[][] map;
    
    public Map(){
        this.map = new MapTile[10][10];
        this.initMap();
    }
    
    private void initMap(){
        for (int col = 0; col < 10; col++) {
            for (int row = 0; row < 10; row++) {
                this.setMapTile(row, col, new WaterTile());
            }
        }
    }
    
    public MapTile getMapTile(int x, int y){
        return this.map[x][y];
    }
    
    public MapTile getMapTile(Coordinates coordinates){
        return this.map[coordinates.getX()][coordinates.getY()];
    }
    
    public void setMapTile(Coordinates coordinates, MapTile tile){
        this.map[coordinates.getX()][coordinates.getY()] = tile;
    }
    
    public void setMapTile(int x, int y, MapTile tile){
        this.map[x][y] = tile;
    }
    
    public MapTile[][] getMapData(){
        return this.map;
    }
    
}

package battleship.model;

public class ShipTile extends MapTile {
    
    private ShipPart shipPart;
    
    public ShipTile(Coordinates coordinates, ShipPart shipPart){
        this.coordinates = coordinates;
        this.shipPart = shipPart;
        this.state = Map.SHIP_HIDDEN;
    }
    
    public ShipPart getShipPart(){
        return this.shipPart;
    }
    
    @Override
    public void hit() {
        System.out.println("HIT CHAIN: ShipTile");
        this.shipPart.setToHit();
        this.setState(Map.SHIP_HIT);
    }
    
}

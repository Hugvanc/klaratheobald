package battleship.model;

import battleship.exceptions.InvalidShipParametersException;
import java.util.ArrayList;
import java.util.List;

public class Player {
    
    private String name;
    private int scores;
    private boolean defeated;
    private int shipCount;
    private List<Ship> ships;
    private Map map;
    
    public Player(String name){
        this.name = name;
        this.scores = 0;
        this.defeated = false;
        
        this.ships = new ArrayList<>();
        this.map = new Map();
        
        try {
            
            this.addShip(new Ship(this, 4));
            this.addShip(new Ship(this, 3));
            this.addShip(new Ship(this, 3));
            this.addShip(new Ship(this, 2));
            this.addShip(new Ship(this, 2));
            this.addShip(new Ship(this, 2));
            this.addShip(new Ship(this, 1));
            this.addShip(new Ship(this, 1));
            this.addShip(new Ship(this, 1));
            this.addShip(new Ship(this, 1));
        } catch (InvalidShipParametersException ex) {
            System.out.println(ex.getMessage());
        }
        this.shipCount = this.ships.size();
    }
    
    public List<Ship> getShips(){
        return this.ships;
    }
    
    public void addShip(Ship ship){
        this.ships.add(ship);
    }
    
    public String getName(){
        return this.name;
    }
    
    public Map getMap(){
        return this.map;
    }
    
    public int getShipCount(){
        return this.shipCount;
    }
    
    public void decShipCount(){
        this.shipCount--;
        //System.out.println("HIT CHAIN: in player.decShipCount() shipCount: 10/"+this.getShipCount());
        if (this.shipCount == 0){
            this.setDefeated();
        }
    }
    
    public void setDefeated(){
        this.defeated = true;
        //System.out.println("HIT CHAIN: in player.serDefeated() " + this.getName() + " is defeated!");
    }
    
    public boolean isDefeated(){
        return this.defeated;
        
    }
    
}

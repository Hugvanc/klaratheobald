package simpleTaks;

//A simple battleship game.

public class Battleship {

    public static int[][] board;
    public static int shipsPlace[][];

    public static void initializeBoard() {
        int row;
        int column;
        do {
            row = extra.Console.readInt("Number of rows: ");
            if (row <= 0) {
                System.out.println("Number of rows must be positive");
            }
        } while (row <= 0);
        do {
            column = extra.Console.readInt("Number of columns: ");
            if (column <= 0) {
                System.out.println("Number of columns must be positive");
            }
        } while (column <= 0);
        board = new int[row][column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                board[i][j] = -1;
            }
        }
    }

    public static void initializeShips() {
        int ships;
        do {
            ships = extra.Console.readInt("Number of ships: 1-" + board.length * board[0].length + " ");
            if (ships <= 0 || ships >= (board.length * board[0].length)) {
                System.out.println("The number of ships cab be 1-" + board.length * board[0].length);
            }
        } while (ships <= 0 || ships >= (board.length * board[0].length));
        shipsPlace = new int[ships][2];
        for (int i = 0; i < ships; i++) {
            boolean isShip;
            do {
                shipsPlace[i][0] = (int) (Math.random() * board.length);
                shipsPlace[i][1] = (int) (Math.random() * board[0].length);
                isShip = false;
                for (int j = 0; j < i; j++) {
                    if (shipsPlace[i][0] == shipsPlace[j][0] && shipsPlace[i][1] == shipsPlace[j][1]) {
                        isShip = true;
                    }
                }
            } while (isShip);
        }

    }

    public static int[] shoot() {
        int[] shoots = new int[2];
        int wichRow;
        int wichColumn;
        do {
            do {
                wichRow = extra.Console.readInt("Enter row: 1-" + board.length+ " ");
                if (wichRow < 1 || wichRow > board.length) {
                    System.out.println("Row can be 1-" + board.length);
                }
            } while (wichRow < 1 || wichRow > board.length);
            do {
                wichColumn = extra.Console.readInt("Enter column: 1-" + board[0].length + " ");
                if (wichColumn < 1 || wichColumn > board.length) {
                    System.out.println("Column can be 1-" + board[0].length);
                }
            } while (wichColumn < 1 || wichColumn > board[0].length);
            if (board[wichRow - 1][wichColumn - 1] != -1) {
                System.out.println("Field had been played before, try again!");
            }
        } while (board[wichRow - 1][wichColumn - 1] != -1);
        shoots[0] = wichRow - 1;
        shoots[1] = wichColumn - 1;
        return shoots;
    }

    public static boolean isShip(int[] shoot) {
        boolean isIt = false;
        for (int i = 0; i < shipsPlace.length && !isIt; i++) {
            if (shipsPlace[i][0] == shoot[0] && shipsPlace[i][1] == shoot[1]) {
                isIt = true;
            }
        }
        return isIt;
    }

    public static void evaluate(int[] shoot) {
        if (isShip(shoot)) {
            board[shoot[0]][shoot[1]] = 1;
        } else {
            board[shoot[0]][shoot[1]] = 0;
        }
    }

    public static boolean finish() {
        boolean isFinish = true;
        for (int i = 0; i < shipsPlace.length; i++) {
            if (board[shipsPlace[i][0]][shipsPlace[i][1]] == -1) {
                isFinish = false;
            }
        }
        return isFinish;
    }

    public static void showBoard() {
        System.out.println("");
        for (int i = 0; i < board[0].length; i++) {
            System.out.print("– ");
        }
        System.out.println("");
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                switch (board[i][j]) {
                    case -1:
                        System.out.print("0 ");
                        break;
                    case 1:
                        System.out.print("X ");
                        break;
                    default:
                        System.out.print("* ");
                        break;
                }
            }
            System.out.println("");
        }
        for (int i = 0; i < board[0].length; i++) {
            System.out.print("– ");
        }
        System.out.println("\n");
    }

    public static void main(String[] args) {
        int gameNumber = 0;
        initializeBoard();
        initializeShips();
        showBoard();
        while (!finish()) {
            evaluate(shoot());
            showBoard();
            gameNumber++;
        }

        System.out.println("Game over. Number of shots" + gameNumber + ".");
    }
}

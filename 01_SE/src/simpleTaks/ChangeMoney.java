package simpleTaks;

//The program prints all of the possible combinations, how a given amount of money can be changed in HUF.

public class ChangeMoney {

    public static void main(String[] args) {
        int cost = extra.Console.readInt("Enter the amount: ");
        int dif = cost % 5;
        if (dif > 0) {
            cost += (dif < 3) ? dif * -1 : 5 - dif;
        }
        int form = cost / 5;
        int digit = 1;
        while (form > 10) {
            form /= 10;
            digit++;
        }

        for (int i = cost / 200; i >= 0; i--) {
            for (int j = (cost - i * 200) / 100; j >= 0; j--) {
                for (int k = (cost - i * 200 - j * 100) / 50; k >= 0; k--) {
                    for (int l = (cost - i * 200 - j * 100 - k * 50) / 20; l >= 0; l--) {
                        for (int m = (cost - i * 200 - j * 100 - k * 50 - l * 20) / 10; m >= 0; m--) {
                            for (int n = (cost - i * 200 - j * 100 - k * 50 - l * 20 - m * 10) / 5; n >= 0 && (i * 200 + j * 100 + k * 50 + l * 20 + m * 10 + n * 5) == cost; n--) {
                                if (i != 0) {
                                    System.out.print(extra.Format.right(i, digit) + "*200");
                                    if (i * 200 != cost) {
                                        System.out.print(" + ");
                                    }
                                }
                                if (j != 0) {
                                    System.out.print(extra.Format.right(j, digit) + "*100");
                                    if (i * 200 + j * 100 != cost) {
                                        System.out.print(" + ");
                                    }
                                }
                                if (k != 0) {
                                    System.out.print(extra.Format.right(k, digit) + "*50");
                                    if (i * 200 + j * 100 + k * 50 != cost) {
                                        System.out.print(" + ");
                                    }
                                }
                                if (l != 0) {
                                    System.out.print(extra.Format.right(l, digit) + "*20");
                                    if (i * 200 + j * 100 + k * 50 + l * 20 != cost) {
                                        System.out.print(" + ");
                                    }
                                }
                                if (m != 0) {
                                    System.out.print(extra.Format.right(m, digit) + "*10");
                                    if (i * 200 + j * 100 + k * 50 + l * 20 + m * 10 != cost) {
                                        System.out.print(" + ");
                                    }
                                }

                                if (n != 0) {
                                    System.out.print(extra.Format.right(n, digit) + "*5 ");
                                }
                                System.out.println(" = " + (i * 200 + j * 100 + k * 50 + l * 20 + m * 10 + n * 5));
                            }
                        }
                    }
                }
            }
        }
    }
}

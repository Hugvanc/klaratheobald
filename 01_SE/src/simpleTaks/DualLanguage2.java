package simpleTaks;

public class DualLanguage2 {

    static String[][] lang = {
        {"Adj meg egy számot! ", "Válassz nyelvet: \n0 - magyar\n1 - angol\n",
            "Kilépés...",
            "Kérlek add meg a kör sugarát: ", "A kör területe: ",
            "Kérem a négyzet oldalát: ", "A négyzet területe: ",
            "Kérem a téglalap egyik oldalát: ", "Kérem a téglalap mésik oldalát: ", "A téglalap területe: ",
            "Nem jó szám. :("},
        {"Declare a number! ", "Select language: \n 0 - hungarian\n 1 - english\n",
            "Exiting",
            "Declare radius: ", "Circle area is: ",
            "Declare side length: ", "Square area is: ",
            "Declare first side length: ", "Decleare second side length: ", "Rectangle area is: ",
            "Wrong number. :("}
    };

    static String[][] menu = {
        {"\nFőmenü\n", "0 - kilep", "1 - nyelv", "2 - kör területe", "3 - négyzet területe", "4 - téglalap területe"},
        {"\nMain menu\n", "0 - exit", "1 - lang", "2 - circle area", "3 - square area", "4 - rectangle area"}
    };

    static int selLang = 0;

    public static String getSelLangElemnt(int i) {
        return lang[selLang][i];
    }

    static void printMenu() {
        for (String str : menu[selLang]) {
            System.out.println(str);
        }
        System.out.println("");
    }

    public static void mainMenu() {
        int mainMenu;
        boolean again = false;
        do {
            printMenu();
            mainMenu = extra.Console.readInt(getSelLangElemnt(0));
            switch (mainMenu) {
                case 0:
                    System.out.println(getSelLangElemnt(2));// exit msg
                    again = false;
                    break;
                case 1:
                    selectLanguage();
                    again = true;
                    break;
                case 2:
                    circleArea();
                    again = true;
                    break;
                case 3:
                    squareArea();
                    again = true;
                    break;
                case 4:
                    recArea();
                    again = true;
                    break;
                default:
                    System.out.println(getSelLangElemnt(10));
                    again = true;
            }
        } while (again);
    }

    private static void selectLanguage() {
        int lang;
        do {
            lang = extra.Console.readInt(getSelLangElemnt(1));
        } while (lang != 0 && lang != 1);
        selLang = lang;
    }

    public static void main(String[] args) {
        mainMenu();
    }

    private static void circleArea() {
        int radius = extra.Console.readInt(getSelLangElemnt(3));
        System.out.println(getSelLangElemnt(4) + Math.pow(radius, 2) * Math.PI);
    }

    private static void squareArea() {
        int side = extra.Console.readInt(getSelLangElemnt(5));
        System.out.println(getSelLangElemnt(6) + Math.pow(side, 2));
    }

    private static void recArea() {
        int side1 = extra.Console.readInt(getSelLangElemnt(7));
        int side2 = extra.Console.readInt(getSelLangElemnt(8));
        System.out.println(getSelLangElemnt(9) + side1 * side2);

    }

}

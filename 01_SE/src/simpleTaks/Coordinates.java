package simpleTaks;

public class Coordinates {

    public static void mainMenu() {
        int mainMenu;
        boolean again = false;
        do {
            System.out.println("Menu: \n\n (0) - Exit \n "
                    + "(1) - Points distance from origo\n "
                    + "(2) - Area defined by three points\n "
                    + "(3) - Chencking four points\n "
                    + "(4) - Reflect triangles\n");
            mainMenu = extra.Console.readInt("Pick a number: ");
            switch (mainMenu) {
                case 0:
                    System.out.println("Exiting...");
                    again = false;
                    break;
                case 1:
                    pointDistance();
                    again = true;
                    break;
                case 2:
                    threePoints();
                    again = true;
                    break;
                case 3:
                    fourPoints();
                    again = true;
                    break;
                case 4:
                    reflection();
                    again = true;
                    break;
                default:
                    System.out.println("Wrong number. :(");
                    again = true;
            }
        } while (again);
    }

    public static void main(String[] args) {
        mainMenu();
    }

    private static void pointDistance() {
        int x = extra.Console.readInt("Enter coordinate x: ");
        int y = extra.Console.readInt("Enter coordinate y: ");
        double distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        System.out.println("The points distance from origo is: " + distance);
    }

    private static void threePoints() {
        int[][] points = new int[3][2];
        double[] sides = new double[3];
        double s = 0;
        double t;
        for (int i = 0; i < points.length; i++) {
            points[i][0] = extra.Console.readInt((i + 1) + " the x coordinate of the point: ");
            points[i][1] = extra.Console.readInt((i + 1) + " the y coordinate of the point: ");
        }
        for (int i = 0; i < sides.length; i++) {
            int j = i + 1;
            if (j == sides.length) {
                j = 0;
            }
            sides[i] = Math.sqrt(Math.pow((points[i][0] - points[j][0]), 2) + Math.pow((points[i][1] - points[j][1]), 2));
            s += sides[i];
        }
        s = s / 2;
        t = Math.sqrt(s * (s - sides[0]) * (s - sides[1]) * (s - sides[2]));
        System.out.println("Area of the triangle: " + t);

    }

    private static void fourPoints() {
        int radius = extra.Console.readInt("Enter circle radius: ");
        int[][] pointData = new int[4][2];
        double[] distances = new double[4];
        int out = 0;
        int on = 0;
        int in = 0;
        for (int i = 0; i < pointData.length; i++) {
            pointData[i][0] = extra.Console.readInt("Enter coordinate x: ");
            pointData[i][1] = extra.Console.readInt("Enter coordinate y: ");
            distances[i] = Math.sqrt(Math.pow(pointData[i][0], 2) + Math.pow(pointData[i][1], 2));
            if (distances[i] > radius) {
                System.out.println("The point is putside of the circle.");
                out++;
            } else if (distances[i] == radius) {
                System.out.println("The point is on the circle.");
                on++;
            } else {
                System.out.println("The point is inside the circle.");
                in++;
            }
        }
        System.out.println("There are "+out + "outside the circle, " + on + " points are on the circle, and " + in + " points are inside the circle.");
    }

    private static void reflection() {
        char m;
        int[][] points = new int[3][4];
        do {
            m = extra.Console.readChar("What would you like to reflect on? X axes, y axes, or origo? ");
            m = java.lang.Character.toLowerCase(m);
            //System.out.println(m);
        } while (m != 'x' && m != 'y' && m != 'o');
        for (int i = 0; i < points.length; i++) {
            points[i][0] = extra.Console.readInt((i + 1) + " the x coordinate of the point: ");
            points[i][1] = extra.Console.readInt((i + 1) + " the y coordinate of the point: ");
            switch (m) {
                case 'x':
                    points[i][2] = points[i][0];
                    points[i][3] = -1 * points[i][1];
                    break;
                case 'y':
                    points[i][2] = -1 * points[i][0];
                    points[i][3] = points[i][1];
                    break;
                default:
                    points[i][2] = -1 * points[i][0];
                    points[i][3] = -1 * points[i][1];
                    break;
            }
        }
        System.out.println("The coordinates of the reflected triangles: " + "(" + points[0][2] + ", " + points[0][3] + "), (" + points[1][2] + ", " + points[1][3] + "), (" + points[2][2] + ", " + points[2][3] + ").");
    }

}

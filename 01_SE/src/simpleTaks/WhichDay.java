package simpleTaks;

//Gives back the number of the day in the year, from en exact date.

public class WhichDay {

    static int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public static boolean leapYear(int year) {
        if (year > 1582 && year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
            return true;
        }
        return false;
    }

    public static int[] date() {
        int[] date = new int[3];
        do {
            date[0] = extra.Console.readInt("Enter year (2010-2019): ");
        } while (date[0] < 2010 || date[0] > 2019);
        if (leapYear(date[0])) {
            months[1] = 29;
        }
        do {
            date[1] = extra.Console.readInt("Enter month (1-12): ");
        } while (date[1] < 1 || date[1] > 12);
        date[1]--;
        do {
            date[2] = extra.Console.readInt("Enter day (1-" + months[date[1]] + "): ");
        } while (date[2] < 1 || date[2] > months[date[1]]);
        return date;
    }

    public static void main(String[] args) {
        int[] dateArray = date();
        int dayCount = 0;
        int dayCount2 = 0;
        for (int i = 2010; i < dateArray[0]; i++) {
            dayCount2 += 365;
            if (leapYear(i)) {
                dayCount2++;
            }
        }
        for (int i = 1; i < dateArray.length; i++) {
            if (i == 1) {
                for (int j = 0; j < dateArray[i]; j++) {
                    dayCount += months[j];
                }
            }
            if (i == 2) {
                dayCount += dateArray[i];
            }

        }
        dayCount2 += dayCount;
        System.out.print("The given date is the " + dayCount + "th day of the year, ");
        switch (dayCount2 % 7) {
            case 1:
                System.out.println("friday.");
                break;
            case 2:
                System.out.println("saturday.");
                break;
            case 3:
                System.out.println("sunday.");
                break;
            case 4:
                System.out.println("monday.");
                break;
            case 5:
                System.out.println("tuesda.");
                break;
            case 6:
                System.out.println("wednesday.");
                break;
            case 7:
                System.out.println("thursday.");
                break;
        }

    }
}

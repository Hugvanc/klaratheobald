package primeThreads;

//Collect big primes on one, and on two threads. Compares the running time.

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class Worker extends Thread {

    static Integer nextNumber = CollectPrimeWithThreads.startNumber;

    @Override
    public void run() {

        while (CollectPrimeWithThreads.addItemToArrayIfPrime(getNextNumber()) < CollectPrimeWithThreads.SIZE) {
        }
    }

    public Integer getNextNumber() {
        int nextN;
        synchronized (nextNumber) {
            nextN = nextNumber++;
        }
        return nextN;
    }

}

public class CollectPrimeWithThreads {

    public static final int SIZE = 4000;
    public static final int startNumber = 20000;
    public static List<Integer> numbers = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println("Starts on one thread...");
        long timeBefore = new Date().getTime();
        int i = startNumber;
        while (addItemToArrayIfPrime(i) < SIZE) {
            i++;
        }
        long timeAfter = new Date().getTime();
        System.out.println("running time:" + (timeAfter - timeBefore) + "\n");
        List<Integer> numbers2 = numbers;
        numbers = new ArrayList<>();
        Worker w1 = new Worker();
        Worker w2 = new Worker();
        System.out.println("Starts on two threads...");
        timeBefore = new Date().getTime();
        w1.start();
        w2.start();
        do {

            try {
                w1.join();
                w2.join();
            } catch (InterruptedException ex) {

            }           
        } while (w2.isAlive() || w1.isAlive());
        timeAfter = new Date().getTime();
        System.out.println("running time:" + (timeAfter - timeBefore));
        System.out.println(numbers.containsAll(numbers2) && numbers2.containsAll(numbers) ? "The two lists are congruent." : "The two lists are different.");

    }

    private static boolean isPrime(int k) {
        int countDividers = 0;
        for (int j = 1; j <= k; j++) {
            if (k % j == 0) {
                countDividers++;
            }
        }
        return countDividers == 2;
    }

    public static int addItemToArrayIfPrime(Integer i) {
        int size;
        boolean prime = isPrime(i);
        synchronized (numbers) {
            if (prime && numbers.size() < SIZE) {
                numbers.add(i);
            }
            size = numbers.size();
        }
        return size;
    }

}

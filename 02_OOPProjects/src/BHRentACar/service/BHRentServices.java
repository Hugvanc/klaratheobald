package BHRentACar.service;

import BHRentACar.utils.BHRentDataBase;
import BHRentACar.utils.BHRentUtils;
import BHRentACar.model.Customer;
import BHRentACar.model.Driver;
import BHRentACar.model.Administrator;
import BHRentACar.model.Car;
import BHRentACar.model.Reservation;
import BHRentACar.model.Rental;
import java.time.LocalDate;

public class BHRentServices {

    public static final int MAXPASSWORDTRY = 3;
    public static final int MINAGE = 18;
    public static final int MAXAGE = 120;
    public static final int MINNUMBEROFSEATS = 2;
    public static final int MAXNUMBEROFSEATS = 12;
    public static final int MINPRICEPERDAY = 3000;
    public static final int MAXPRICEPERDAY = 10000;

    public static int findName(String emailAdress) {
        int i = 0;

        while (i < BHRentDataBase.admins.size() && !(BHRentDataBase.admins.get(i).getEmailAdress().equals(emailAdress))) {
            i++;
        }
        return i < BHRentDataBase.admins.size() ? i : -1;
    }

    public static int findPassword(String password) {
        int i = 0;
        while (i < BHRentDataBase.admins.size() && !(BHRentDataBase.admins.get(i).getAdminPassword().equals(password))) {
            i++;
        }
        return i < BHRentDataBase.admins.size() ? i : -1;
    }

    public static int AdminLogin() {
        boolean emialAdressOk = false;
        boolean passwordOk = false;
        int i;
        String emailAdress;
        String password;
        do {
            emailAdress = extra.Console.readLine("E-mail adress: ");
            //uName;
            i = findName(emailAdress);
            if (i == -1) {
                System.out.println(emailAdress);
                System.out.println("Invalid e-mail adress!");
            } else {
                emialAdressOk = true;
            }
        } while (!emialAdressOk);
        int countTry = 0;
        do {
            ++countTry;
            password = extra.Console.readLine("Password:");
            //uName;
            i = findPassword(password);
            if (i == -1) {
                System.out.println(password);
                System.out.println("Invalid password " + (MAXPASSWORDTRY - countTry) + " attempts left.");

            } else {
                passwordOk = true;
            }
            if (countTry == 3) {
                System.out.println("Invalid login, try again\n");
            }
        } while (!passwordOk && countTry < 3);
        int findAdmin = -1;
        for (int j = 0; j < BHRentDataBase.admins.size(); j++) {
            if (BHRentDataBase.admins.get(j).getEmailAdress().equals(emailAdress)) {
                findAdmin = j;
            }
        }
        return findAdmin;
    }

    public static void createRental() {
        BHRentDataBase.rentals.add(new Rental(extra.Console.readLine("Enter rental adress: ")));
    }

    public static void createCar() {
        BHRentUtils.listArrayList(BHRentDataBase.rentals);
        int witchRental = extra.Console.readInt("Enroll car to a rental. Chose ID. Available rentals: \n"
                + BHRentDataBase.rentals);
        if (witchRental < BHRentDataBase.rentals.size() - 1 && BHRentDataBase.rentals.get(witchRental) != null) {
            BHRentDataBase.allCars.add(new Car(
                    extra.Console.readLine("Enter license plate number: "),
                    extra.Console.readLine("Enter cartype: "),
                    checkedSeats(),
                    extra.Console.readLine("Enter color: "),
                    extra.Console.readInt("Enter price/day: ")));
            BHRentDataBase.rentals.get(witchRental).storeCar(BHRentDataBase.allCars.get(BHRentDataBase.allCars.size() - 1));
        }
    }

    public static void createAdmin() {
        BHRentDataBase.admins.add(new Administrator(
                extra.Console.readLine("Enter name: "),
                checkedYear(),
                extra.Console.readLine("Enter e-mail adress: "),
                extra.Console.readLine("Enter phonenumber: ")));
    }

    public static void createCustomer() {
        BHRentDataBase.customers.add(new Customer(
                extra.Console.readLine("Enter name: "),
                checkedYear(),
                extra.Console.readLine("Enter e-mail adress: "),
                extra.Console.readLine("Enter phonenumber: ")));
    }

    public static void createDriver() {
        BHRentDataBase.drivers.add(new Driver(
                extra.Console.readLine("Enter licence number: "),
                extra.Console.readLine("Enter name: "),
                checkedYear(),
                extra.Console.readLine("Enter e-mail adress: "),
                extra.Console.readLine("Enter phonenumber: ")));
    }

    public static void createReservation() {
        BHRentDataBase.reservations.add(new Reservation(//customer, driver, startRental, targetRental, LocalDate.MIN, LocalDate.MIN));
                (Customer) BHRentUtils.choseFromList(BHRentDataBase.customers, "customer"),
                (Driver) BHRentUtils.choseFromList(BHRentDataBase.drivers, "driver"),
                (Rental) BHRentUtils.choseFromList(BHRentDataBase.rentals, "rental"),
                (Rental) BHRentUtils.choseFromList(BHRentDataBase.rentals, "rental"),
                checkedDate(),
                checkedDate()));
    }

    public static void fulfill() {
        Reservation reservation = (Reservation) BHRentUtils.choseFromList(BHRentDataBase.reservations, "reservation");
        reservation.getStartRental().loseCar(reservation.getCar());
        reservation.getTargetRental().storeCar(reservation.getCar());
    }

    public static LocalDate checkedDate() {
        LocalDate date;
        do {
            date = LocalDate.of(extra.Console.readInt("Enter year of birth"),
                    extra.Console.readInt("Enter month 1-12:"),
                    extra.Console.readInt("Enter day:"));
            if (date.isBefore(LocalDate.now())) {
                System.out.println("Can't make a reservation retroactively.");
            }
        } while (date.isBefore(LocalDate.now()));
        return date;
    }

    public static LocalDate checkedYear() {
        LocalDate dateOfBirth;
        do {
            dateOfBirth = LocalDate.of(extra.Console.readInt("Enter year of birth"),
                    extra.Console.readInt("Enter month 1-12:"),
                    extra.Console.readInt("Enter day:"));
            if (BHRentUtils.calculateAge(dateOfBirth) < MINAGE) {
                System.out.println("Customer is to young, he/she has to be 18 yeras old to rent a car.");
            }
            if (BHRentUtils.calculateAge(dateOfBirth) > MAXAGE) {
                System.out.println("Age is too high, probably bad imput.");
            }

        } while (BHRentUtils.calculateAge(dateOfBirth) < MINAGE && BHRentUtils.calculateAge(dateOfBirth) > MAXAGE);
        return dateOfBirth;
    }

    public static int checkedSeats() {
        int seats;
        do {
            seats = extra.Console.readInt("Enter the number of seats (2-12): ");
            if (!(seats >= MINNUMBEROFSEATS && seats <= MAXNUMBEROFSEATS)) {
                System.out.println("Nomber of seats is not valid.");
            }
        } while (!(seats >= MINNUMBEROFSEATS && seats <= MAXNUMBEROFSEATS));
        return seats;
    }

    public static void changeAdminPassword(Administrator admin) {
        String previousPassword;
        int countTry = 0;
        do {
            previousPassword = extra.Console.readLine("Please enter previous password.");
            if (!(previousPassword.equals(admin.getAdminPassword()))) {
                System.out.println("Invalid password, try again! " + (MAXPASSWORDTRY - countTry) + " attempts left.");
                countTry++;
            }
        } while (!(previousPassword.equals(admin.getAdminPassword())) && countTry < MAXPASSWORDTRY);

        if (countTry > MAXPASSWORDTRY) {
            System.out.println("You are not entitled to change this password.");
        } else {
            admin.setAdminPassword(extra.Console.readLine("Please enter the new password"));
            BHRentUtils.passwords.set(BHRentUtils.passwords.indexOf(previousPassword), admin.getAdminPassword());
        }
    }

}

package BHRentACar.model;

import BHRentACar.utils.BHRentUtils;
import BHRentACar.utils.Increment;
import java.time.LocalDate;

public class Administrator extends Person {

    private int adminID;
    private String adminPassword;

    private static final String ALPHA_NUMERIC_STRING = "abcdefghijklmopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public Administrator(String name, LocalDate dateOfBirth, String emailAdress, String phoneNumber) {
        super(name, dateOfBirth, emailAdress, phoneNumber);
        this.adminID = Increment.countAdmin++;
        this.adminPassword = BHRentUtils.generatePassword();

    }

    public int getAdminID() {
        return adminID;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    @Override
    public String getEmailAdress() {
        return super.getEmailAdress(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getAge() {
        return super.getAge(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "Administrator{" + "adminID=" + adminID + super.toString() + ", adminPassword=" + adminPassword + '}';
    }

}

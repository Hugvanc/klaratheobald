package BHRentACar.model;

import BHRentACar.utils.Increment;
import java.util.ArrayList;

public class Rental {

    private int rentalID;
    private String adress;
    public static ArrayList<Car> cars = new ArrayList<>();

    public Rental(String adress) {
        this.adress = adress;
        this.rentalID = Increment.countRental++;;

    }

    public void storeCar(Car c) {
        cars.add(c);
    }

    public void loseCar(Car c) {
        cars.remove(c);
    }

    @Override
    public String toString() {
        return "Rental{" + "rentalID=" + rentalID + ", adress=" + adress + "}";
    }

}

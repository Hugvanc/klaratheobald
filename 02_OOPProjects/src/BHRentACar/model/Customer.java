package BHRentACar.model;

import BHRentACar.utils.Increment;
import java.time.LocalDate;
import java.util.ArrayList;

public class Customer extends Person {

    private int id;
    private long CardNumber;
    private ArrayList<Reservation> reservations = new ArrayList<>();

    public Customer(String name, LocalDate dateOfBirth, String emailAdress, String phoneNumber) {
        super(name, dateOfBirth, emailAdress, phoneNumber);
        this.id = Increment.countCustomer++;

    }

    @Override
    public String toString() {
        return "Customer{" + super.toString() + "id=" + id + ", CardNumber=" + CardNumber + '}';
    }

}

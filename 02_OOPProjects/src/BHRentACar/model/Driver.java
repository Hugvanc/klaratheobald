package BHRentACar.model;

import BHRentACar.utils.Increment;
import java.time.LocalDate;

public class Driver extends Person {

    private String licenceNumber;
    private int id;

    public Driver(String licenceNumber, String name, LocalDate dateOfBirth, String emailAdress, String phoneNumber) {
        super(name, dateOfBirth, emailAdress, phoneNumber);
        this.licenceNumber = licenceNumber;
        this.id = Increment.countDriver++;

    }

    @Override
    public String toString() {
        return "Driver{" + "licenceNumber=" + licenceNumber + ", id=" + id + '}';
    }

}

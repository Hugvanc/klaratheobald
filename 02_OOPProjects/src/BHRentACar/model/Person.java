package BHRentACar.model;

import BHRentACar.utils.BHRentUtils;
import java.time.LocalDate;

public abstract class Person {

    private String name;
    private LocalDate dateOfBirth;
    private int age;
    private String emailAdress;
    private String phoneNumber;

    public Person(String name, LocalDate dateOfBirth, String emailAdress, String phoneNumber) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.age = BHRentUtils.calculateAge(dateOfBirth);
        this.emailAdress = emailAdress;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        return " name=" + name + ", dateOfBirth=" + dateOfBirth + ", age=" + age + ", emailAdress=" + emailAdress + ", phoneNumber=" + phoneNumber;
    }

}

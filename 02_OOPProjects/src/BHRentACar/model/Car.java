package BHRentACar.model;

import BHRentACar.utils.Increment;

public class Car {

    private int carID;
    private String licensePlate;
    private String carType;
    private int numberOfSeats;
    private String color;
    private int pricePerDay;

    public Car(String licensePlate, String carType, int numberOfSeats, String color, int pricePerDay) {
        this.carID = Increment.countCar++;
        this.carType = carType;
        this.numberOfSeats = numberOfSeats;
        this.color = color;
        this.licensePlate = licensePlate;
        this.pricePerDay = pricePerDay;
    }

    public int getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(int pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCarID() {
        return carID;
    }

    public String getCarType() {
        return carType;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    @Override
    public String toString() {
        return "Car{" + "carID=" + carID + ", carType=" + carType + ", numberOfSeats=" + numberOfSeats + ", color=" + color + '}';
    }

}

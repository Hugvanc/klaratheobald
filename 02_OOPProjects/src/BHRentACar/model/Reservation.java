package BHRentACar.model;

import BHRentACar.utils.Increment;
import java.time.LocalDate;
import java.time.Period;

public class Reservation {

    private int reservationId;
    private Customer customer;
    private Driver driver;
    private Rental startRental, targetRental;
    private LocalDate startingDay, endinDay;
    private Car car;
    private int price;

    public Reservation(Customer customer, Driver driver, Rental startRental, Rental targetRental, LocalDate startingDay, LocalDate endinDay) {
        this.reservationId = Increment.countReservation++;
        this.customer = customer;
        this.driver = driver;
        this.car = car;
        this.startRental = startRental;
        this.targetRental = targetRental;
        this.startingDay = startingDay;
        this.endinDay = endinDay;
        this.price = calculatePrice();
    }

    private int calculatePrice() {
        int days = Period.between(startingDay, endinDay).getDays() + 1;
        return days * car.getPricePerDay();
    }

    public Rental getStartRental() {
        return startRental;
    }

    public Rental getTargetRental() {
        return targetRental;
    }

    public Car getCar() {
        return car;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Reservation{" + "reservationId=" + reservationId + ", customer=" + customer + ", driver=" + driver + ", startRental=" + startRental + ", targetRental=" + targetRental + ", startingDay=" + startingDay + ", endinDay=" + endinDay + ", car=" + car + ", price=" + price + '}';
    }

}

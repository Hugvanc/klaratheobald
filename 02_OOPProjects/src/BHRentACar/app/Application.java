package BHRentACar.app;

import BHRentACar.model.Administrator;
import BHRentACar.utils.BHRentDataBase;
import BHRentACar.service.BHRentServices;
import BHRentACar.utils.BHRentUtils;
import BHRentACar.model.Car;
import BHRentACar.model.Rental;
import java.time.LocalDate;

public class Application {

    public static void main(String[] args) {
        BHRentDataBase.admins.add(new Administrator("Adminisztrátor Adrás", LocalDate.of(1980, 1, 30), "adminBandi@bhrent.hu", "+36-70-544-4637"));
        BHRentDataBase.admins.add(new Administrator("Adminisztrátor Adrás", LocalDate.of(1980, 1, 30), "adminBandi@bhrent.hu", "+36-70-544-4637"));
        BHRentDataBase.rentals.add(new Rental("Budapest, Üllői út 12."));
        BHRentDataBase.rentals.add(new Rental("Budapest, Klapka utca 7."));
        BHRentDataBase.allCars.add(new Car("HSO-547", "Toyota", 4, "zöld", 4000));
        BHRentDataBase.allCars.add(new Car("MGR-643", "Renault", 5, "kék", 4500));
        BHRentDataBase.rentals.get(0).storeCar(BHRentDataBase.allCars.get(0));
        BHRentDataBase.rentals.get(0).storeCar(BHRentDataBase.allCars.get(1));
        BHRentDataBase.allCars.add(new Car("HUG-767", "Suzuki", 4, "fehér", 3000));
        BHRentDataBase.allCars.add(new Car("MJA-698", "Smart", 2, "kék", 2500));
        BHRentDataBase.rentals.get(1).storeCar(BHRentDataBase.allCars.get(2));
        BHRentDataBase.rentals.get(1).storeCar(BHRentDataBase.allCars.get(3));
        BHRentUtils.listArrayList(BHRentDataBase.admins);

        //BHRentServices.findName("adminBandi@bhrent.hu");
        System.out.println(BHRentDataBase.admins.get(0).getEmailAdress());
        rentalMenu();
    }

    public static void rentalMenu() {
        System.out.println("*****************************");
        System.out.println("Rent a car administration, please login!");
        System.out.println("*****************************");
        BHRentServices.AdminLogin();
        System.out.println("Welcome! Please select menu: \n"
                + "(1) Rental administration (Create car, rental, ...)\n"
                + "(2) Customer arministration (Creat customer, driver, reservation ...)\n"
                + "(0) Exit");
        int choseMenu;
        do {
            choseMenu = extra.Console.readInt("Chose an option(0-2): ");
            if (choseMenu < 0 || choseMenu > 2) {
                System.out.println("Not valid option.");
            }
        } while (choseMenu < 0 || choseMenu > 2);
        switch (choseMenu) {
            case 1:
                rentalAdminMenu();
                break;
            case 2:
                customerAdminMenu();
                break;
            case 0:
                break;
        }

    }

    public static void rentalAdminMenu() {
        System.out.println("Rental administration: \n"
                + "(1) Create rental\n"
                + "(2) Create car\n"
                + "(3) Create administrator\n"
                + "(4) Create lists\n"
                + "(0) Go back");
        int choseMenu;
        do {
            choseMenu = extra.Console.readInt("Chose an option(0-4): ");
            if (choseMenu < 0 || choseMenu > 4) {
                System.out.println("Not valid option.");
            }
        } while (choseMenu < 0 || choseMenu > 4);
        switch (choseMenu) {
            case 1:
                BHRentServices.createRental();
                break;
            case 2:
                BHRentServices.createCar();
                break;
            case 3:
                BHRentServices.createAdmin();
                break;
            case 4:
                listMenu();
                break;
            case 0:
                break;
        }
    }

    public static void customerAdminMenu() {
        System.out.println("Customer administration menu: \n"
                + "(1) Create customer\n"
                + "(2) Create driver\n"
                + "(3) Create reservation\n"
                + "(4) Create file fullfillment\n"
                + "(0) Go back");
        int choseMenu;
        do {
            choseMenu = extra.Console.readInt("Chose an option(0-4): ");
            if (choseMenu < 0 || choseMenu > 4) {
                System.out.println("Not valid option.");
            }
        } while (choseMenu < 0 || choseMenu > 4);
        switch (choseMenu) {
            case 1:
                BHRentServices.createCustomer();
                break;
            case 2:
                BHRentServices.createDriver();
                break;
            case 3:
                BHRentServices.createReservation();
                break;
            case 4:
                BHRentServices.fulfill();
                break;
            case 0:
                break;
        }

    }

    public static void listMenu() {
        System.out.println("Administrative listing menu: \n"
                + "(1) list admininstrators\n"
                + "(2) list rentals\n"
                + "(3) list cars\n"
                + "(4) list customers\n"
                + "(5) list drivers\n"
                + "(6) reservations\n"
                + "(0) Go back");
        int choseMenu;
        do {
            choseMenu = extra.Console.readInt("Chose an option(0-6): ");
            if (choseMenu < 0 || choseMenu > 6) {
                System.out.println("Not valid option.");
            }
        } while (choseMenu < 0 || choseMenu > 6);
        switch (choseMenu) {
            case 1:
                BHRentUtils.listArrayList(BHRentDataBase.admins);
                break;
            case 2:
                BHRentUtils.listArrayList(BHRentDataBase.rentals);
                break;
            case 3:
                BHRentUtils.listArrayList(BHRentDataBase.allCars);
                break;
            case 4:
                BHRentUtils.listArrayList(BHRentDataBase.customers);
                break;
            case 5:
                BHRentUtils.listArrayList(BHRentDataBase.drivers);
                break;
            case 6:
                BHRentUtils.listArrayList(BHRentDataBase.reservations);
                break;
            case 0:
                break;
        }

    }

}

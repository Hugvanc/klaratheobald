package BHRentACar.utils;

import BHRentACar.model.Customer;
import BHRentACar.model.Driver;
import BHRentACar.model.Administrator;
import BHRentACar.model.Car;
import BHRentACar.model.Reservation;
import BHRentACar.model.Rental;
import java.util.ArrayList;

public class BHRentDataBase {

    public static ArrayList<Administrator> admins = new ArrayList<>();
    public static ArrayList<Rental> rentals = new ArrayList<>();
    public static ArrayList<Car> allCars = new ArrayList<>();
    public static ArrayList<Customer> customers = new ArrayList<>();
    public static ArrayList<Driver> drivers = new ArrayList<>();
    public static ArrayList<Reservation> reservations = new ArrayList<>();

}

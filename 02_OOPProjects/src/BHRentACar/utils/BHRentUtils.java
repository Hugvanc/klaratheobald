package BHRentACar.utils;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

public class BHRentUtils {

    private static final String ALPHA_NUMERIC_STRING = "abcdefghijklmopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int PASSWORDCARACTERCOUNT = 8;
    public static ArrayList<String> passwords = new ArrayList<>();

    public static String randomAlphaNumeric() {
        int count = PASSWORDCARACTERCOUNT;
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();

    }

    public static String generatePassword() {
        int validNumber;
        String validPassword;
        boolean isValidPassword = true;
        do {
            validPassword = randomAlphaNumeric();
            for (String password : passwords) {
                if (validPassword.equals(password)) {
                    isValidPassword = false;
                }
            }
        } while (!isValidPassword);
        passwords.add(validPassword);
        return validPassword;
    }

    public static int calculateAge(LocalDate birthDate) {
        LocalDate currentDate = LocalDate.now();
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

    public static Object choseFromList(ArrayList list, String str) {
        int whichObject;
        listArrayList(list);
        do {
            whichObject = extra.Console.readInt("Please choose " + str + "(1-" + list.size() + "): ");
            if (whichObject > list.size() || whichObject < 1) {
                System.out.println("Invalid selection");
            }
        } while (whichObject > list.size() || whichObject < 1);
        return list.get(--whichObject);
    }

    public static void listArrayList(ArrayList list) {
        int i = 0;
        for (Object object : list) {
            System.out.println((i + 1) + ". " + list.get(i));
            i++;
        }
    }

}

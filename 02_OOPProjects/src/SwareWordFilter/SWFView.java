package SwareWordFilter;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class SWFView extends JFrame {

    private SWFController controller;
    private JTextArea taEditor = new JTextArea();
    private JButton btTest = new JButton("Test");
    private JButton btClear = new JButton("Clear");
    private JComboBox cbLang = new JComboBox();

    public SWFView(SWFController controller) throws HeadlessException {
        this.controller = controller;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 500);
        setLocationRelativeTo(this);

        JPanel mainPanel = new JPanel(new FlowLayout());
        JPanel panel = new JPanel(new GridLayout(7, 1));
        JPanel pnlang = new JPanel();

        String[] boxText = controller.generateComboBoxElements();
        for (int i = 0; i < boxText.length; i++) {
            String str = boxText[i];
            cbLang.addItem(str);
        }
        cbLang.setSelectedItem(controller.getActualLanguage());
        JPanel jp1 = new JPanel();
        panel.add(jp1);
        panel.add(cbLang);
        JPanel pntest = new JPanel();
        panel.add(pntest);
        panel.add(btTest);
        JPanel pnclear = new JPanel();
        panel.add(pnclear);
        panel.add(btClear);
        mainPanel.add(panel);
        add(mainPanel, BorderLayout.LINE_START);
        add(new JScrollPane(taEditor));
        //add(taEditor);

        cbLang.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.selectLanguage(cbLang.getSelectedItem().toString());
            }
        });
        btTest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                taEditor.setText(controller.getCheckedText(taEditor.getText()));
            }
        });
        btClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                taEditor.setText("");
            }
        });
        setVisible(true);
    }
}

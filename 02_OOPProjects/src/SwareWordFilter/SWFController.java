package SwareWordFilter;

import java.util.HashSet;

public class SWFController {

    private SWFModel model;

    public SWFController(SWFModel model) {
        this.model = model;
    }

    public String[] generateComboBoxElements() {
        return model.getLanguageNames();
    }

    public void selectLanguage(String str) {
        model.chooseWordSet(str);
    }

    public String getCheckedText(String str) throws NullPointerException {
        if (str == null) {
            throw new IllegalArgumentException("No string found");
        }
        HashSet<String> curseWordSet = model.getActualWordSet();
        if (curseWordSet == null) {
            throw new IllegalStateException("No hasset found");
        }
        StringBuilder sb = new StringBuilder(str);
        final char[] stars = new char[]{'*', '*', '*'};
        for (String curseWord : curseWordSet) {
            int indexOfCurseWord;
            while ((indexOfCurseWord = sb.indexOf(curseWord)) != -1) {
                sb.delete(indexOfCurseWord, indexOfCurseWord + curseWord.length());
                if (indexOfCurseWord >= sb.length()) {
                    sb.append(stars);
                } else {
                    sb.insert(indexOfCurseWord, stars);
                }

            }
        }
        return sb.toString();
    }

    String getActualLanguage() {
        return model.getActualKey();
    }

}

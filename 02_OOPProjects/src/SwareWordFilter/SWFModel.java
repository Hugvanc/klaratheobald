package SwareWordFilter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SWFModel {

    private HashMap<String, HashSet> lang = new HashMap<>();
    private HashSet<String> actualWordSet;
    private String actualKey;

    public SWFModel() {
        String fileName;
        HashSet hs;
        for (File file : getFileList()) {
            hs = readLanguageFile(file);
            if (!hs.isEmpty()) {
                fileName = file.getName();
                lang.put(fileName.substring(0, fileName.length() - 4), hs);
            }
        }
        actualKey = "-";
        lang.put(actualKey, new HashSet<String>());
        chooseWordSet(actualKey);
    }

    public HashSet<String> getActualWordSet() {
        return actualWordSet;
    }

    public HashMap<String, HashSet> getLang() {
        return lang;
    }

    public void chooseWordSet(String key) {
        actualWordSet = lang.get(key);
        actualKey = key;
    }

    public String getActualKey() {
        return actualKey;
    }

    private HashSet readLanguageFile(File f) {
        HashSet hs = new HashSet();
        if (f.exists()) {
            try {
                FileReader fr = new FileReader(f);

                BufferedReader bfr = new BufferedReader(fr);
                try {
                    String line;
                    while ((line = bfr.readLine()) != null) {
                        hs.add(line);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(SWFModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SWFModel.class.getName()).log(Level.SEVERE, "Nincs ilyen file", ex);

            }
        }
        return hs;
    }

    public List<File> getFileList() {
        File f = new File("./data");
        List<File> fileList = new ArrayList<>();
        if (f.exists() && f.isDirectory()) {
            File[] files = f.listFiles();
            for (File file : files) {
                if (file.isFile() && file.getName().endsWith(".txt")) {
                    fileList.add(file);
                }
            }
        }
        return fileList;
//        for (int i = 0; i < files.length; i++) {
//            System.out.println(files[i].getName());
//        }

    }

    String[] getLanguageNames() {
        Set<String> keys = lang.keySet();
        String[] names = new String[keys.size()];
        int i = 0;
        for (String key : keys) {
            names[i++] = key;
        }
        return names;
    }
}

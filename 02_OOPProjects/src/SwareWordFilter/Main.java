package SwareWordFilter;

public class Main {

    public static void main(String[] args) {
        SWFModel model = new SWFModel();
        SWFController controller = new SWFController(model);
        SWFView view = new SWFView(controller);
    }
}

package UncleJohns;

public class JohnsPubTest {

    public static void pubMenu() {
        if (!pubClosed()) {
            char charMenu;
            do {
                charMenu = extra.Console.readChar("This is a pub simulator, for informations press i.\n"
                        + "\nThe pub is now open. "
                        + "\nWhat is happening in the pub?"
                        + "\nIf someone orderes a drink, press d."
                        + "\nIf you want to know who the youngest person is, press y."
                        + "\nIf you want to know which drink contains the less alcohol, press a."
                        + "\nIf you are leaving, press e.\n");
                if (charMenu == 'i') {
                    System.out.println("You can simulate a simple pub."
                            + "\nPeople are ordering drinks, getting drunk, spend their money, "
                            + "\nor drink all the wine or soda in the pub.");
                }
            } while (charMenu != 'd' && charMenu != 'y' && charMenu != 'a' && charMenu != 'e');
            if (charMenu == 'e') {
                System.out.println("Good bye!");
            } else {
                switch (charMenu) {
                    case 'd':
                        drinkOrder();
                        break;
                    case 'y':
                        youngest();
                        break;
                    case 'a':
                        lessAlcohol();
                        break;
                }
                pubMenu();
            }
        } else {
            System.out.println("The pub is closed, good bye.");
        }
    }

    static JohnsPub pub = new JohnsPub(20, 20);
    static Person gandalf = new Person("Gandalf", 5678, 5000, 0);
    static Person aragorn = new Person("Aragorn", 92, 5000, 0);
    static Person boromir = new Person("Boromir", 45, 4000, 30);
    static Person frodo = new Person("Frodo", 50, 1000, 0);
    static Person pippin = new Person("Pippin", 17, 1000, 0);
    static Person[] fellowship = {gandalf, aragorn, boromir, frodo, pippin};

    static Drink smallDrink = new Drink("Small drink", 1, 1);
    static Drink mediumDrink = new Drink("Medium drink", 2, 1);
    static Drink largeDrink = new Drink("Large drink", 3, 2);
    static Drink wineOnly = new Drink("Wine", 3, 0);
    static Drink justSoda = new Drink("Soda", 0, 3);
    static Drink noDrink = new Drink("noDrink", 0, 0);
    static Drink[] drinks = {smallDrink, mediumDrink, largeDrink, wineOnly, justSoda};

    public static void main(String[] args) {
        pubMenu();
    }

    private static boolean pubClosed() {
        int drunkards = 0;
        for (int i = 0; i < fellowship.length - 1; i++) {
            if (fellowship[i].getGotAlcohol() >= 40) {
                drunkards++;
            }
        }
        return drunkards >= 4
                || pub.getWine() <= 0 || pub.getSoda() <= 0
                || gandalf.getMoney() == 0 && aragorn.getMoney() == 0 && boromir.getMoney() == 0
                && frodo.getMoney() == 0 && pippin.getMoney() == 0;
    }

    private static Drink order(Person p) {
        char drink;
        do {
            drink = extra.Console.readChar("What is he drinking."
                    + "\npress s for " + smallDrink
                    + "\npress m for " + mediumDrink
                    + "\npress l for " + largeDrink
                    + "\npress w for " + wineOnly
                    + "\npress j for " + justSoda);
        } while (drink != 's' && drink != 'm' && drink != 'l' && drink != 'w' && drink != 'j');

        if (p.getGotAlcohol() < 40) {
            switch (drink) {
                case 's':
                    if (p.getMoney() >= smallDrink.getPrice() && pub.getWine() >= 1 && pub.getSoda() >= 1) {
                        return smallDrink;
                    } else {
                        System.out.println("No money or drink left.");
                        return noDrink;
                    }
                case 'm':
                    if (p.getMoney() >= mediumDrink.getPrice() && pub.getWine() >= 2 && pub.getSoda() >= 1) {
                        return mediumDrink;
                    } else {
                        System.out.println("No money or drink left.");
                        return noDrink;
                    }
                case 'l':
                    if (p.getMoney() >= largeDrink.getPrice() && pub.getWine() >= 3 && pub.getSoda() >= 2) {
                        return largeDrink;
                    } else {
                        System.out.println("No money or drink left.");
                        return noDrink;
                    }
                case 'w':
                    if (p.getMoney() >= wineOnly.getPrice() && pub.getWine() >= 3) {
                        return wineOnly;
                    } else {
                        System.out.println("No money or drink left.");
                        return noDrink;
                    }
                default:
                    if (p.getMoney() >= justSoda.getPrice() && pub.getSoda() >= 3) {
                        return justSoda;
                    } else {
                        System.out.println("No money or drink left.");
                        return noDrink;
                    }
            }
        } else {
            System.out.println("He's drunk, only can have soda.");
            if (pub.getSoda() > 3) {
                return justSoda;
            } else {
                System.out.println("No more soda");
                return noDrink;
            }
        }
    }

    private static void drinkOrder() {

        char person;
        do {
            person = extra.Console.readChar("Who wants to drink? "
                    + "\npress g for Gandalf,"
                    + "\npress a for Aragorn,"
                    + "\npress b for Boromir,"
                    + "\npress f for Frodo,"
                    + "\npress p for Pippin.\n");
        } while (person != 'g' && person != 'a' && person != 'b' && person != 'f' && person != 'p');

        if (person == 'p') {
            if (pub.getSoda() >= 3) {
                if (pippin.getMoney() > justSoda.getPrice()) {
                    System.out.println("He can only drink soda.");
                    pub.giveDrink(justSoda, pippin);
                } else {
                    System.out.println("He doesn't have enough money.");
                }
            } else {
                System.out.println("No more soda :(");
            }
        } else {
            switch (person) {
                case 'g':
                    pub.giveDrink(order(gandalf), gandalf);
                    break;
                case 'a':
                    pub.giveDrink(order(aragorn), aragorn);
                    break;
                case 'b':
                    pub.giveDrink(order(boromir), boromir);
                    break;
                case 'f':
                    pub.giveDrink(order(frodo), frodo);
                    break;
            }
        }
        System.out.println(pub.getWine() + " wine, and " + pub.getSoda() + " soda left.\n");
    }

    private static void youngest() {
        System.out.println(Utils.getYoungestPerson(fellowship));
    }

    private static void lessAlcohol() {
        System.out.println(Utils.getDrinkWithLowestAlcholContent(drinks));
    }

}

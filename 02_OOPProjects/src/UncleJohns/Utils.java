package UncleJohns;

public class Utils {

    public static Drink getDrinkWithLowestAlcholContent(Drink[] drinks) {
        double alcMin = 1;
        int minIndex = 0;
        for (int i = 0; i < drinks.length; i++) {
            if (drinks[i].getWine() / drinks[i].getSize() < alcMin) {
                alcMin = drinks[i].getWine() / drinks[i].getSize();
                minIndex = i;
            }
        }
        if (alcMin == Integer.MAX_VALUE) {
            return null;
        }
        return drinks[minIndex];
    }

    public static Person getYoungestPerson(Person[] persons) {
        int ageMin = Integer.MAX_VALUE;
        int minIndex = 0;
        for (int i = 0; i < persons.length; i++) {
            if (persons[i].getAge() < ageMin) {
                ageMin = persons[i].getAge();
                minIndex = i;
            }
        }
        if (ageMin == Integer.MAX_VALUE) {
            return null;
        }
        return persons[minIndex];
    }
}

package UncleJohns;

public class JohnPubTest2 {

    public static void pubMenu() {
        if (!pubClosed()) {
            char charMenu;
            do {
                charMenu = extra.Console.readChar("This is a pub simulator, for informations press i.\n"
                        + "\nThe pub is now open. "
                        + "\nWhat is happening in the pub?"
                        + "\nIf someone orderes a drink, press d."
                        + "\nIf you want to know who the youngest person is, press y."
                        + "\nIf you want to know which drink contains the less alcohol, press a."
                        + "\nIf you are leaving, press e.\n");
                if (charMenu == 'i') {
                    System.out.println("You can simulate a simple pub."
                            + "\nPeople are ordering drinks, getting drunk, spend their money, "
                            + "\nor drink all the wine or soda in the pub.");
                }
            } while (charMenu != 'd' && charMenu != 'y' && charMenu != 'a' && charMenu != 'e');
            if (charMenu == 'e') {
                System.out.println("Good bye!");
            } else {
                switch (charMenu) {
                    case 'd':
                        drinkOrder();
                        break;
                    case 'y':
                        youngest();
                        break;
                    case 'a':
                        lessAlcohol();
                        break;
                }
                pubMenu();
            }
        } else {
            System.out.println("The pub is closed, good bye.");
        }
    }

    static JohnsPub pub = new JohnsPub(20, 20);
    static Person gandalf = new Person("Gandalf", 5678, 5000, 0);
    static Person aragorn = new Person("Aragorn", 92, 5000, 0);
    static Person boromir = new Person("Boromir", 45, 4000, 0);
    static Person frodo = new Person("Frodo", 50, 4000, 0);
    static Person pippin = new Person("Pippin", 17, 1000, 0);
    static Person[] fellowship = {gandalf, aragorn, boromir, frodo, pippin};

    static Drink smallDrink = new Drink("Small drink", 1, 1);
    static Drink mediumDrink = new Drink("Medium drink", 2, 1);
    static Drink largeDrink = new Drink("Large drink", 3, 2);
    static Drink wineOnly = new Drink("Wine", 4, 0);
    static Drink justSoda = new Drink("Soda", 0, 3);
    static Drink noDrink = new Drink("noDrink", 0, 0);
    static Drink[] drinks = {smallDrink, mediumDrink, largeDrink, wineOnly, justSoda};

    public static void main(String[] args) {
        pubMenu();
    }

    private static boolean pubClosed() {
        int drunkards = 0;
        for (int i = 0; i < fellowship.length - 1; i++) {
            if (fellowship[i].getGotAlcohol() >= 40) {
                drunkards++;
            }
        }
        return drunkards >= 4
                || pub.getWine() <= 0 || pub.getSoda() <= 0
                || gandalf.getMoney() == 0 && aragorn.getMoney() == 0 && boromir.getMoney() == 0
                && frodo.getMoney() == 0 && pippin.getMoney() == 0;
    }

    private static Drink order(Person p) {
        char drink;
        do {
            drink = extra.Console.readChar("What is he drinking."
                    + "\npress s for small drink,"
                    + "\npress m for medium drink,"
                    + "\npress l for large drink,"
                    + "\npress w for wine,"
                    + "\npress j for just soda.");
        } while (drink != 's' && drink != 'm' && drink != 'l' && drink != 'w' && drink != 'j');
        if (p.getMoney() > 0) {
            switch (drink) {
                case 's':
                    return smallDrink;
                case 'm':
                    return mediumDrink;
                case 'l':
                    return largeDrink;
                case 'w':
                    return wineOnly;
                default:
                    return justSoda;
            }
        } else {

            return noDrink;
        }
    }

    private static void drinkOrder() {

        char person;
        do {
            person = extra.Console.readChar("Who wants to drink? "
                    + "\npress g for Gandalf,"
                    + "\npress a for Aragorn,"
                    + "\npress b for Boromir,"
                    + "\npress f for Frodo,"
                    + "\npress p for Pippin.\n");
        } while (person != 'g' && person != 'a' && person != 'b' && person != 'f' && person != 'p');

        if (person == 'p') {
            if (pub.getSoda() > 0) {
                System.out.println("He can only drink soda.");
                pub.giveDrink(justSoda, pippin);
            } else {
                System.out.println("No more soda :(");
            }
        } else {
            switch (person) {
                case 'g':
                    pub.giveDrink(order(gandalf), gandalf);
                    break;
                case 'a':
                    pub.giveDrink(order(aragorn), aragorn);
                    break;
                case 'b':
                    pub.giveDrink(order(aragorn), boromir);
                    break;
                case 'f':
                    pub.giveDrink(order(aragorn), frodo);
                    break;
            }

        }
        System.out.println(pub.getWine() + " wine, and " + pub.getSoda() + " soda left");
    }

    private static void youngest() {
        System.out.println(Utils.getYoungestPerson(fellowship));
    }

    private static void lessAlcohol() {
        System.out.println(Utils.getDrinkWithLowestAlcholContent(drinks));
    }

}

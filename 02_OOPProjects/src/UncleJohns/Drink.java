package UncleJohns;

public class Drink {

    private int soda, wine, price, size;
    private String name;

    public Drink(String name, int wine, int soda) {
        this.name = name;
        this.soda = soda;
        this.wine = wine;
        this.price = this.soda * SODAPRICE + this.wine * WINEPRICE;
        this.size = this.soda + this.wine;
    }
    private static final int WINEPRICE = 300;
    private static final int SODAPRICE = 50;

    public int getSoda() {
        return soda;
    }

    public int getWine() {
        return wine;
    }

    public int getPrice() {
        return price;
    }

    public int getSize() {
        return size;
    }

    public int alcoholMl(Drink d) {
        return d.wine * 10;
    }

    @Override
    public String toString() {
        return name + ": soda=" + soda + " dl, wine=" + wine + " dl, price=" + price + " Huf";
    }

}

package UncleJohns;

public class Person {

    private int age, money, gotAlcohol;
    private String name;

    public Person(String name, int age, int money, int gotAlcohol) {
        this.name = name;
        this.age = age;
        this.money = money;
        this.gotAlcohol = gotAlcohol;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(Drink d) {
        this.money = money - d.getPrice();
    }

    public int getGotAlcohol() {
        return gotAlcohol;
    }

    public void setGotAlcohol(Drink d) {
        this.gotAlcohol += d.alcoholMl(d);
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return this.name + " is " + this.age + " years old.";
    }

}

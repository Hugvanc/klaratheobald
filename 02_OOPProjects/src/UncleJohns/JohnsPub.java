package UncleJohns;

public class JohnsPub {

    private int wine, soda;

    public JohnsPub(int wine, int soda) {
        this.wine = wine;
        this.soda = soda;
    }

    public void setWine(Drink d) {
        wine -= d.getWine();
    }

    public void setSoda(Drink d) {
        soda -= d.getSoda();
    }

    public int getWine() {
        return wine;
    }

    public int getSoda() {
        return soda;
    }

    public void giveDrink(Drink d, Person p) {
        setWine(d);
        setSoda(d);
        p.setGotAlcohol(d);
        p.setMoney(d);

    }

}

package ticTacToe;

import java.util.ArrayList;
import java.util.List;

public class TicTacToeService {

    private static int[] hitList = new int[9];
    private int[] possibleRowSums = {-2, 2, -1, 1};// ha a sorok értékének összege ennyi, akkor érdemes tenni ide.
    private List<Integer> computerToShoot = new ArrayList<>();
    private int countX = 0;
    private int countO = 0;

    public int computerShoots() {
        int shootIndex;
        if (hitList[4] == 0) {//ha a négyes vagyis a középső szabad akkor oda teszi
            shootIndex = 4;
        } else {
            for (int i = 0; i < possibleRowSums.length; i++) {//megnézi a sorok összegét, ha talál olyat ami megfelel a posibleRowSumsban, akkor beteszi egy arraylistbe, amiből a végén az első szabad helyre tesz
                for (int j = 0; j < hitList.length; j += 3) {
                    if (hitList[j] + hitList[j + 1] + hitList[j + 2] == possibleRowSums[i]) {//sorok
                        computerToShoot.add(j);
                        computerToShoot.add(j + 1);
                        computerToShoot.add(j + 2);
                        System.out.print(possibleRowSums[i]);
                    }
                }
                for (int j = 0; j < 3; j++) {
                    if (hitList[j] + hitList[j + 3] + hitList[j + 6] == possibleRowSums[i]) {//oszlopok
                        computerToShoot.add(j);
                        computerToShoot.add(j + 3);
                        computerToShoot.add(j + 6);
                        System.out.print(possibleRowSums[i]);
                    }
                }
                if (hitList[0] + hitList[4] + hitList[8] == possibleRowSums[i]) {//egyik átló
                    computerToShoot.add(0);
                    computerToShoot.add(4);
                    computerToShoot.add(8);
                    System.out.print(possibleRowSums[i]);
                }
                if (hitList[2] + hitList[4] + hitList[6] == possibleRowSums[i]) {//másik átló
                    computerToShoot.add(2);
                    computerToShoot.add(4);
                    computerToShoot.add(6);
                    System.out.print(possibleRowSums[i]);
                }
            }
            System.out.println("");
            if (computerToShoot.isEmpty()) {//ha üres a lista akkor random tesz egy szabad helyre
                do {
                    shootIndex = (int) (Math.random() * 9);
                } while (hitList[shootIndex] != 0);
            } else {//ha nem üres akkor tömbben lévő első szabad indexre tesz
                int i = 0;
                do {
                    shootIndex = computerToShoot.get(i);
                    i++;
                } while (hitList[shootIndex] != 0);
            }
        }
        System.out.println(computerToShoot.toString());
        computerToShoot.clear();//üríti a tömböt minden lövés után
        return shootIndex;
    }

    public void setHitListX(int i) {
        hitList[i] = 1;
        countX++;

    }

    public void setHitListO(int i) {
        hitList[i] = -1;
        countO++;
    }

    public void writeHitlist() {
        for (int i = 0; i < hitList.length; i++) {
            System.out.print(hitList[i] + ", ");
        }
        System.out.println("");
    }

    public boolean isFreeSpace() {
        boolean isFree = false;
        for (int i = 0; i < hitList.length; i++) {
            if (hitList[i] == 0) {
                isFree = true;
            }
        }
        return isFree;
    }

    public int isAWinner() {//ha sorban, oszlopban vagy átlóban megegyeznek az értékek, akkor megjegyzi
        //az egyik indexet, megnézi hogy hányas szám van benne, és visszadja a számot, és ez alapján be
        //lehet azonosítani hogy ki nyert
        int winnerIndex = 0;
        for (int i = 0; i < 9; i += 3) {
            if (hitList[i] != 0 && hitList[i] == hitList[i + 1] && hitList[i + 1] == hitList[i + 2]) {
                winnerIndex = hitList[i];
            }
        }
        for (int i = 0; i < 3; i++) {
            if (hitList[i] != 0 && hitList[i] == hitList[i + 3] && hitList[i + 3] == hitList[i + 6]) {
                winnerIndex = hitList[i];
            }
        }
        if (hitList[0] != 0 && hitList[0] == hitList[4] && hitList[4] == hitList[8]) {
            winnerIndex = hitList[0];
        }
        if (hitList[2] != 0 && hitList[2] == hitList[4] && hitList[4] == hitList[6]) {
            winnerIndex = hitList[2];
        }
        return winnerIndex;

    }

    public int getCountX() {
        return countX;
    }

    public int getCountO() {
        return countO;
    }

    public void clearHitlist() {
        for (int i = 0; i < hitList.length; i++) {
            hitList[i] = 0;
        }
    }
}

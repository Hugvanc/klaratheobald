package ticTacToe;

import javax.swing.JButton;
import javax.swing.JOptionPane;

public class TicTacToeController {

    private TicTacToeBoard board;
    private TicTacToeService service = new TicTacToeService();

    public TicTacToeController(TicTacToeBoard b) {
        this.board = b;
    }

    void markButtonWithXFirst(JButton jButton) {//játékos kezd
        if (service.isFreeSpace() && service.isAWinner() == 0) {
            jButton.setText("X");
            service.setHitListX(board.getIndexOfButton(jButton));
            if (service.isFreeSpace() && service.isAWinner() == 0) {
                int compShoots = service.computerShoots();
                markButtonWithO(board.getButtonByIndex(compShoots));
                service.setHitListO(compShoots);
            }
        }
        service.writeHitlist();
        endGame();
    }


    void endGame() {//nyertesvizsgálat
        if (service.isFreeSpace() && service.isAWinner() != 0) {
            if (service.isAWinner() == 1) {
                finalMessage("You won out of " + service.getCountX() + " steps.", board);
            } else {
                finalMessage("Computer won out of " + service.getCountX() + " steps.", board);
            }
            service.clearHitlist();
        }
        if (!service.isFreeSpace() && service.isAWinner() == 0) {
            finalMessage("Game over, it's a draw.", board);
            service.clearHitlist();
        }

    }

    void markButtonWithO(JButton jButton
    ) {
        jButton.setText("O");
    }


    public void finalMessage(String str, TicTacToeBoard ticTacToeBoard) {//vége ablak, kilépés vagy új játék
        String[] choices = {"New game", "Exit"};
        int response = JOptionPane.showOptionDialog(ticTacToeBoard, str + "Do you want to play a new game?", "Game over", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
        if (response == 0) {
            new TicTacToeBoard();
        } else {
            Runtime.getRuntime().exit(0);
        }
    }

}

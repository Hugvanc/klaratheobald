package ticTacToe;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class TicTacToeBoard extends JFrame implements ActionListener {

    private JButton[] buttons = new JButton[9];
    private TicTacToeController controller = new TicTacToeController(this);

    public TicTacToeBoard() throws HeadlessException {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300, 300);
        setLocationRelativeTo(this);
        setLayout(new GridLayout(3, 3));
        for (int i = 0; i < 9; i++) {
            buttons[i] = new JButton();
            buttons[i].addActionListener(this);
            add(buttons[i]);
        }

        setVisible(true);
    }

    public JButton[] getButtons() {
        return buttons;
    }

    @Override
    public void actionPerformed(ActionEvent e) {     
        controller.markButtonWithXFirst((JButton) e.getSource());
    }

    public JButton getButtonByIndex(int index) {
        return buttons[index];
    }

    public int getIndexOfButton(JButton jb) {
        int index = -1;
        for (int i = 0; i < 9; i++) {
            if (buttons[i].equals(jb)) {
                index = i;
            }
        }
        return index;

    }

}
